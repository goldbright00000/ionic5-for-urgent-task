import { Component, NgZone } from '@angular/core';
import { Platform, ToastController, NavController } from '@ionic/angular';
import { StatusBar } from '@ionic-native/status-bar/ngx';
import { Location } from '@angular/common';
import { Router, NavigationExtras, NavigationEnd } from '@angular/router';
import { Storage } from '@ionic/storage';
import { GlobalService } from './services/global.service';
import { Globalization } from '@ionic-native/globalization/ngx';
import { TranslateService } from '@ngx-translate/core';
import { Device } from '@ionic-native/device/ngx';
import { SplashScreen } from '@ionic-native/splash-screen/ngx';
import { Network } from '@ionic-native/network/ngx';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import * as pac from 'package.json';
import { PlanItemPage } from './pages/plan-item/plan-item.page';
import { CityGuideItemPage } from './pages/city-guide-item/city-guide-item.page';
import { CityAgendaItemPage } from './pages/city-agenda-item/city-agenda-item.page';
import { OffersDiscoutnsItemPage } from './pages/offers-discoutns-item/offers-discoutns-item.page';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html'
})
export class AppComponent {
  toast: any;
  toast1: any;
  canNavigate:boolean = true;
  constructor(
    private platform: Platform,
    private statusBar: StatusBar,
    private location: Location,
    private router: Router,
    private storage: Storage,
    private _globalServ: GlobalService,
    private globalization: Globalization,
    private translate: TranslateService,
    private device: Device,
    private splashScreen: SplashScreen,
    private network: Network,
    private toastCtrl: ToastController,
    private ga: GoogleAnalytics,
    private navController: NavController,    
    private ngZone: NgZone,    
  ) {
    let disconnectSubscription = this.network.onDisconnect().subscribe(() => {
      console.log('network was disconnected :-(');
      this.showToast('No internet connection', null, 'red');
    });

    // stop disconnect watch
    //disconnectSubscription.unsubscribe();


    // watch network for a connection
    let connectSubscription = this.network.onConnect().subscribe(() => {
      console.log('network connected!');

      setTimeout(() => {
        console.log('we got a wifi connection, woohoo!');
        // this.showToast1(' Internet Connection established', 2000, 'green');

      }, 2000);
      // We just got a connection but we need to wait briefly
      // before we determine the connection type. Might need to wait.
      // prior to doing any api requests as well.


    });
    this.initializeApp();
  }

  initializeApp() {
    this.platform.ready().then(() => {
     // this.splashScreen.hide();
      branchInit();      
      this.ga.startTrackerWithId('UA-148439747-1')
       .then(() => {       
           this.ga.setAppVersion(pac.version);                                
           this.router.events.subscribe((event)=>{
           if(event instanceof NavigationEnd) {
            this.ga.trackView(event.urlAfterRedirects);
           }              
         });                             
         })
       .catch(e => console.log('Error starting GoogleAnalytics', e));

      this.statusBar.styleDefault();

      // document.addEventListener('backbutton', (evt: any) => {
      //   if (this._globalServ.idUser != null) {
      //     this.location.back();
      //   }
      // });


      this.getDivice();
      this.storage.get('language').then((val) => {
        console.log('Language', val)
        if (val == null) {
          this.getLanguage();
        } else {
          this.translate.setDefaultLang(val);
          this._globalServ.language = val;
        }
      });
      this.storage.get('token').then((val) => {
        if (val != null) {
          this.storage.get('idUser').then((val) => {
            this.splashScreen.hide();
            this._globalServ.idUser = val;
            if(this.canNavigate)
            this.router.navigate(['tabs/plan-list'], {})
          });
        } else {
          this.splashScreen.hide();
          if(this.canNavigate)
          this.router.navigate(['start'], {})
        }
      });
    });

    this.platform.resume.subscribe(() => {
      branchInit();
    });

    // Branch initialization
    const branchInit = () => {
      // only on devices      
      if (!this.platform.is("cordova")) {
        console.log("Branch Init 2")
        return;
      }      
      const Branch = window["Branch"];
      Branch.initSession().then(data => {                
        if (data["+clicked_branch_link"]) {          
          this.ngZone.run(()=>{     
            this.storage.get('token').then((val) => {
              if (val != null) {
                this.storage.get('idUser').then((val) => {
                  this.splashScreen.hide();
                  this._globalServ.idUser = val;
                  this.canNavigate = false;
                  this.router.navigateByUrl(data["$canonical_identifier"]); 
                });
              } else {
                this.splashScreen.hide();
                this.canNavigate = false;
                this.router.navigate(['login'], {})
              }
            });                            
          })          
        }
      });
    };


  }
  async showToast(msg, dur, css) {
    if (this.toast == undefined) {
      this.toast = await this.toastCtrl.create({
        message: msg,
        duration: dur,
        cssClass: css,
        showCloseButton: true,

      })
      await this.toast.present();
    }
    this.toast.onDidDismiss().then(res => {
      this.toast = undefined;
    })
  }
  async showToast1(msg, dur, css) {
    if (this.toast)
     await this.toast.dismiss();
    if (this.toast1 == undefined) {
      this.toast1 = await this.toastCtrl.create({
        message: msg,
        duration: dur,
        cssClass: css,
        showCloseButton: true,

      })
      await this.toast1.present();
    }
    this.toast1.onDidDismiss().then(res => {
      this.toast1 = undefined;
    })
  }
  getDivice() {
    if (this.platform.is('ios')) {
      this._globalServ.iosDivice = true;
    }
    if (this.platform.is('android')) {
      this._globalServ.androidDivice = true;
    }
    console.log('model -->', this.device.model);
  }

  getLanguage() {
    this.globalization.getPreferredLanguage()
      .then(res => {
        this._globalServ.language = res.value.substring(0, 2);
        console.log('language:', this._globalServ.language);
        if (this._globalServ.language == 'en' || this._globalServ.language == 'es') {
          this.translate.setDefaultLang(this._globalServ.language);
        } else {
          this.translate.setDefaultLang('en');
        }
      }).catch(error => {
        this.translate.setDefaultLang('en');
        this._globalServ.language = 'en';
        console.log(error)
      });
  }

}
