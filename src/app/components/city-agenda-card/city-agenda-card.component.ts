import { Component, OnInit, Input } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { environment } from '../../../environments/environment';
import { GlobalService } from '../../services/global.service';

@Component({
  selector: 'city-agenda-card',
  templateUrl: './city-agenda-card.component.html',
  styleUrls: ['./city-agenda-card.component.scss'],
})
export class CityAgendaCardComponent implements OnInit {
  placeHolder: string = './assets/img/placeholder.gif';
  @Input() dataCAItem: any;
  url: any = environment.apiUrl;

  constructor(
    private router: Router,
    public _globalServ: GlobalService,
  ) { }

  ngOnInit() {
    console.log(this._globalServ.language);
  }

  goToCityAgendaItem(value: any) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': value.id })
      },
    };
    this.router.navigate(['city-agenda-item'], navigationExtras)
  }

  short_text(name) {
    var text = name.slice(0, 82) + "(...)";
    return text;
  }
  getDate(plandate) {
    if (plandate) {
      const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
        "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
      ];
      let dateCommon = new Date(plandate).toDateString();

      let day = dateCommon.slice(0, dateCommon.indexOf(' '));
      let date: any = new Date(plandate).getDate();
      let month: any = monthNames[new Date(plandate).getMonth()];
      let year: any = new Date(plandate).getFullYear();
      let hours: any = new Date(plandate).getUTCHours();
      let minutes: any = new Date(plandate).getUTCMinutes();
      if (minutes < 10)
        minutes = '0' + minutes;
      else
        minutes = '' + minutes;

      return day + ', ' + date + ' ' + month + ' ' + year + ', ' + hours + ':' + minutes
    }
  }
}
