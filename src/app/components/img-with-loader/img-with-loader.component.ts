import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-img-with-loader',
  templateUrl: './img-with-loader.component.html',
  styleUrls: ['./img-with-loader.component.scss'],
})
export class ImgWithLoaderComponent implements OnInit {

  @Input('src') src: string = '';
  @Input('class') class: string = '';
  @Output('click') click: EventEmitter<any> = new EventEmitter<any>();
  @Output('error') error: EventEmitter<any> = new EventEmitter<any>();
  @Input('spinnerWidth') spinnerWidth: string = '15%';
  loading:boolean = true;


  constructor() { }

  ngOnInit() {

  }

}
