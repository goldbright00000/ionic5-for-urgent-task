import { Component, OnInit, Input, } from '@angular/core';
import { Router, NavigationExtras } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { environment } from '../../../environments/environment';
import { UserService } from '../../services/user.service';
import { PlanService } from '../../services/plan.service';
import { GroupService } from '../../services/group.service';
import { ToastController } from '@ionic/angular';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { get } from 'lodash';
import { PlanListPage } from "../../pages/plan-list/plan-list.page";
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
	selector: 'plan-card',
	templateUrl: './plan-card.component.html',
	styleUrls: ['./plan-card.component.scss'],
})
export class PlanCardComponent implements OnInit {
	placeHolder: string = './assets/img/placeholder.gif';
	@Input() dataPlan: any;
	url: any = environment.apiUrl;
	dataUser: any;
	getParticipants: any;
	getJoin: any;
	getUserLoggedin: any;
	public joined: boolean = false;

	constructor(
		private router: Router,
		public _globalServ: GlobalService,
		private _userServ: UserService,
		private _planServ: PlanService,
		private _groupServ: GroupService,
		private toastController: ToastController,
		private ga: GoogleAnalytics,
		private iab: InAppBrowser, public planListPage: PlanListPage
	) {

	}

	async ngOnInit() {
		if (this.dataPlan && this.dataPlan.type !== 'club_plan') {
			if (this._globalServ.org == 'IE') {
				await this._userServ.getProfile(this.dataPlan.fk_ie_user_id, null)
					.subscribe(data => {
						this.dataUser = data;
					}, error => {
						console.log(error);
					});


			} else if (this._globalServ.org == 'CEU') {

				await this._userServ.getProfile(this.dataPlan.fk_ceu_user_id, null)
					.subscribe(data => {
						this.getParticipants = data;
						console.log(this.getParticipants)
					}, error => {
						console.log(error);
					});
			}
			await this._planServ.getParticipants(this.dataPlan.id, { 'participantlimit': 5, 'entity': this._globalServ.org })
				.subscribe(data => {
					this.getParticipants = data;
					this._globalServ.getParticipants = data;
					this.checkingJoinStatus();
				}, error => {
					console.log(error);
				});
		}


	}

	async goToItemPlan() {
		if (this.dataPlan.type !== 'group_plan') {

			console.log(this.dataPlan);
			//change yagnesh
			if (this.dataPlan.private && !this.dataPlan.can_access_private_plan && !this.checkJoinedUser()) {
				const toast = await this.toastController.create({
					message: 'You do not have access to this private plan',
					duration: 2000
				})
				toast.present();
			}

			else {
				let navigationExtras: NavigationExtras = {
					queryParams: {
						data: JSON.stringify({ 'id': this.dataPlan.id, 'type': this.dataPlan.type })
					},
				};
				this.router.navigate(['plan-item'], navigationExtras)
			}
		}

	}

	goToEventLink() {
		if (get(this.dataPlan, 'eventLink', '') !== '') {
			this.iab.create(get(this.dataPlan, 'eventLink', ''), '_system');
		}
	}

	join() {
		console.log("PLan type: ", this.dataPlan.type);
		if (this.dataPlan.type == 'club_plan') {
			if (this.joined == true) {
				this._planServ.leavePlanClub(this.dataPlan.idParticipation).subscribe(data => {
					console.log(data);
					this.joined = false;
					this.dataPlan.idParticipation = null;
					this._planServ.getDataPlanList();
					this.ga.trackEvent("Club Plan - " + this.dataPlan.title, "Left");
				}, error => {
					console.log(error);
				})
			} else {
				this._userServ.getProfile(this._globalServ.idUser, {}).subscribe(data => {
					this.getUserLoggedin = data;
					this._planServ.joinPlanClub({ 'fk_plan_id': this.dataPlan.id, 'ceu_user_id': this._globalServ.idUser, 'name': this.getUserLoggedin.user.first_name + ' ' + this.getUserLoggedin.user.last_name, 'email': this.getUserLoggedin.user.email, 'fk_club_plan_id': this.dataPlan.id }).subscribe(data => {
						console.log(data);
						this.getJoin = data;
						this.joined = true;
						this.dataPlan.idParticipation = this.getJoin.id;
						this._planServ.getDataPlanList();
						this.ga.trackEvent("Club Plan - " + this.dataPlan.title, "Joined");
					}, error => {
						console.log(error);
					})
				});
			}
		}
		if (this.dataPlan.type == 'university_plan' || this.dataPlan.type == 'my_plan' || this.dataPlan.type == 'student_plan') {
			console.log("idParticipation", this.dataPlan.idParticipation);
			if (this.joined == true) {
				this._planServ.leavePlanUniversity(this.dataPlan.idParticipation).subscribe(data => {
					console.log(data);
					this.joined = false;
					this.dataPlan.idParticipation = null;
					this._planServ.getDataPlanList();
					let plan = this.dataPlan.type == 'university_plan' ? 'University Plan' : (this.dataPlan.type == 'my_plan' ? 'My Plan' : 'Student Plan');
					this.ga.trackEvent(plan + ' - ' + this.dataPlan.title, "Left");
					if (this.dataPlan.type == 'my_plan') {
						this.router.navigate(['tabs/plan-list']);
						this.planListPage.showMyPlans();
					}
				}, error => {
					console.log(error);
				})
			} else {
				this._userServ.getProfile(this._globalServ.idUser, {}).subscribe(data => {
					this.getUserLoggedin = data;
					if (this._globalServ.org == 'CEU') {
						this._planServ.joinPlanUniversity({
							'fk_plan_id': this.dataPlan.id,
							'ceu_user_id': this._globalServ.idUser,
							'name': this.getUserLoggedin.user.first_name + ' ' + this.getUserLoggedin.user.last_name,
							'email': this.getUserLoggedin.user.email
						}).subscribe(data => {
							console.log('ceu s:', data);
							this.getJoin = data;
							this.joined = true;
							this.dataPlan.idParticipation = this.getJoin.id;
							this._planServ.getDataPlanList();
							let plan = this.dataPlan.type == 'university_plan' ? 'University Plan' : (this.dataPlan.type == 'my_plan' ? 'My Plan' : 'Student Plan');
							this.ga.trackEvent(plan + ' - ' + this.dataPlan.title, "Joined");
							if (this.dataPlan.type == 'my_plan') {
								this.router.navigate(['tabs/plan-list']);
								this.planListPage.showMyPlans();
							}
						}, error => {
							console.log('ceu e:', error);
						})
					} else if (this._globalServ.org == 'IE') {
						this._planServ.joinPlanUniversity({
							'fk_ie_plan_id': this.dataPlan.id,
							'ie_user_id': this._globalServ.idUser,
							'name': this.getUserLoggedin.user.first_name + ' ' + this.getUserLoggedin.user.last_name,
							'email': this.getUserLoggedin.user.email
						}).subscribe(data => {
							console.log('i s:', data);
							this.getJoin = data;
							this.joined = true;
							this.dataPlan.idParticipation = this.getJoin.id;
							let plan = this.dataPlan.type == 'university_plan' ? 'University Plan' : (this.dataPlan.type == 'my_plan' ? 'My Plan' : 'Student Plan');
							this.ga.trackEvent(plan + ' - ' + this.dataPlan.title, "Joined");
							this._planServ.getDataPlanList();
							if (this.dataPlan.type == 'my_plan') {
								this.router.navigate(['tabs/plan-list']);
								this.planListPage.showMyPlans();
							}
						}, error => {
							console.log('i e:', error);
						})
					}
				});
			}
		}
		if (this.dataPlan.type == 'group_plan') {
			this._userServ.getProfile(this._globalServ.idUser, {}).subscribe(data => {
				this.getUserLoggedin = data;
				this._groupServ.joinGroupPlan(this.dataPlan.id, { 'userId': this._globalServ.idUser, 'email': this.getUserLoggedin.user.email }).subscribe(async data => {
					console.log(data);
					this.getJoin = data;
					this.joined = true;
					this.dataPlan.idParticipation = this.getJoin.id;
					const toast = await this.toastController.create({
						message: 'Joined successfully.',
						duration: 2000
					});
					toast.present();
					this.ga.trackEvent("Group Plan - " + this.dataPlan.title, "Joined");
				}, async error => {
					console.log(error);
					const toast = await this.toastController.create({
						message: error.message,
						duration: 2000
					});
					toast.present();
				})
			});
		}
	}
	getDate(plandate) {
		if (plandate) {
			const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
				"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
			];
			let dateCommon = new Date(plandate).toDateString();

			let day = dateCommon.slice(0, dateCommon.indexOf(' '));
			let date = new Date(plandate).getDate();
			let month = monthNames[new Date(plandate).getMonth()];
			let year = new Date(plandate).getFullYear();
			let hours = new Date(plandate).getUTCHours();
			let minutes: any = new Date(plandate).getUTCMinutes();
			if (minutes < 10)
				minutes = '0' + minutes;
			else
				minutes = '' + minutes;
			return day + ', ' + date + ' ' + month + ' ' + year + ', ' + hours + ':' + minutes
		}
	}
	openPublicProfile(user) {
		console.log(user);
		let navExtra: NavigationExtras = {
			queryParams: {
				data: JSON.stringify({
					id: user.user.id,
					email: user.user.email
				})
			}
		}
		this.router.navigate(['/view-profile'], navExtra);
	}

	getParticipantId() {

		for (var i = 0; i < this.dataPlan.IE_Participants.length; i++) {
			if (this.dataPlan.IE_Participants[i].ie_user_id == this._globalServ.idUser) {
				return this.dataPlan.IE_Participants[i].id;
			}
		}
	}
	checkJoinedUser() {
		if (this.getParticipants) {
			for (let i = 0; i < this.getParticipants.participants.length; i++) {
				if (this.getParticipants.participants[i].ie_user_id == this._globalServ.idUser) {
					return true;
				}
			}
		}
	}

	checkingJoinStatus() {
		this._planServ.getParticipations(this._globalServ.idUser).subscribe(data => {
			let listParticipations = JSON.parse(JSON.stringify(data));
			for (let item2 of listParticipations) {
				let itemParticipants = this._globalServ.org == 'CEU' ? item2.Participants : item2.IE_Participants;
				for (let item3 of itemParticipants) {
					let planId = this._globalServ.org == 'CEU' ? item3.fk_plan_id : item3.fk_ie_plan_id;
					if (planId == this.dataPlan.id) {
						this.joined = true;
						this.dataPlan.idParticipation = item3.id;
					}
				}
			}
		});
	}

}
