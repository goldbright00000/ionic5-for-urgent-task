import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { GeneralService } from '../../services/general.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { AlertController, Platform } from '@ionic/angular';
import {EmailComposer} from "@ionic-native/email-composer/ngx";

@Component({
  selector: 'app-about',
  templateUrl: './about.page.html',
  styleUrls: ['./about.page.scss'],
})
export class AboutPage implements OnInit {
  pageTitleName: any = false;

  constructor(
    public _globalServ: GlobalService,
    public _generalServ: GeneralService,
    private iab: InAppBrowser,
    private callNumber: CallNumber,
    public alertController: AlertController,
    private platform : Platform,
    private emailComposer: EmailComposer
    ) { }

  ngOnInit() {
  }

  logScrolling(e){
    if(e.detail.scrollTop>50){
      this.pageTitleName = true;
    } else {
      this.pageTitleName = false;
    }
  }

  async callWithNumber() {
    const alert = await this.alertController.create({
      subHeader: '34921412410',
      buttons: [
      {
        text: 'Cancel',
        role: 'cancel',
        cssClass: 'secondary',
        handler: (blah) => {
          console.log('Confirm Cancel: blah');
        }
      }, {
        text: 'Call',
        handler: () => {
          console.log('Confirm Okay');
          this.callNumber.callNumber("34921412410", true)
          .then(res => console.log('Launched dialer!', res))
          .catch(err => console.log('Error launching dialer', err));
        }
      }
      ]
    });

    await alert.present();
  }

  openWebVistingo() {
    // this.iab.create('https://campuslife-events.ie.edu/home', '_system')
    // this.iab.create('https://www.ie.edu/privacy-policy/', '_system');

    let lat = 40.4389003,lng = -3.6911568;
    let destination = lat + ',' + lng;
    if(this.platform.is('ios')){
      window.open('maps://?q=' + destination, '_system');
    } else {
      let label = encodeURI('My Label');
      window.open('geo:0,0?q=' + destination + '(' + label + ')', '_system');
    }
    
  }

  openPrivacyPolicy(){
    // this.iab.create('https://campuslife-events.ie.edu/home', '_system')
    this.iab.create('https://www.ie.edu/privacy-policy/', '_system');

  }

    sendEmail() {
        // window.open('mailto:university@ie.edu');
        let email = {
            to: 'info@vistingo.com',
            subject: 'App Support'
        }
        this.emailComposer.open(email);
    }

}
