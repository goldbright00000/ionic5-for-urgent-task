import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Location } from '@angular/common';
import { GlobalService } from '../../services/global.service';
import { } from 'googlemaps';
import { CityAgendaService } from '../../services/city-agenda.service';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Router, NavigationExtras } from '@angular/router';
import { get } from 'lodash';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { Calendar } from '@ionic-native/calendar/ngx';
import { ModalController, NavController, ToastController } from '@ionic/angular';
import { ImageModalPage } from '../image-modal/image-modal.page';
import { TruncateModule } from '@yellowspot/ng-truncate';
import {SocialSharing} from "@ionic-native/social-sharing/ngx";
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { BranchIOService } from 'src/app/services/branch-io.service';

@Component({
	selector: 'app-city-agenda-item',
	templateUrl: './city-agenda-item.page.html',
	styleUrls: ['./city-agenda-item.page.scss'],
})
export class CityAgendaItemPage implements OnInit {
	truncating = true;
	@Input() text: string;
	@Input() limit: number = 250;
	latitudeInit: any;
	longitudeInit: any;
	zoom: any = 14;
	getItem: any;
	id: any;
	url: any = environment.apiUrl;
	read_flag: boolean;

	@ViewChild("map") mapElement;
	AddedToCalender: boolean = false;

	constructor(
		private router: Router,
		public location: Location,
		public _globalServ: GlobalService,
		private _cityAgendaServ: CityAgendaService,
		private actRoute: ActivatedRoute,
		private iab: InAppBrowser,
		private calendar: Calendar,
		public modalController: ModalController,
		public navCtrl: NavController,
		 private toastCtrl: ToastController,
		  private socialShare: SocialSharing,
		  public ga: GoogleAnalytics,
		  public branch: BranchIOService,
		) {
		this.read_flag = false;
		if (this.actRoute.snapshot.queryParams.data != null) {
			let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
			this.id = getParams.id;
		}
	}

	ngOnInit() {
	}

	ionViewDidEnter() {
		this._cityAgendaServ.getItem(this.id)
			.subscribe(data => {
				this.getItem = data;
				this.ga.trackEvent("City Agenda - "+this.getItem.event.name_EN, "Viewed");
				console.log("777",this.getItem);
				let openingDatetime = [];
				let openingtimeArray;
				if (this._globalServ.language == 'en') {
					openingtimeArray = this.getItem.event.Places[0].hours_EN.split('\n');
					console.log("/////////",openingtimeArray)
				}
				else {
					openingtimeArray = this.getItem.event.Places[0].hours_ES.split('\n');

				}
				for (var i = 0; i < openingtimeArray.length; ++i) {
					if (openingtimeArray[i] != '')
						// openingDatetime.push(openingtimeArray[i].split(': ')[0], openingtimeArray[i].split(': ')[1]);
						openingDatetime.push(openingtimeArray[i].split(': ')[0],openingtimeArray[i].split(': ')[1]);

				}
				console.log(">>>",openingDatetime);
				this.getItem.event.hours = openingDatetime;

				// 
				// this.getItem.event.Places[0].hours_EN
				// 
				for (let item of this.getItem.event.Places) {
					this.latitudeInit = Number(item.latitude);
					this.longitudeInit = Number(item.longitude);
					break;
				}
				//set map
				if(this.latitudeInit && this.longitudeInit) {
					let coords = new google.maps.LatLng(this.latitudeInit, this.longitudeInit);
					let mapOptions: google.maps.MapOptions = {
						center: coords,
						zoom: this.zoom,
						maxZoom: 17,
						minZoom: 3,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						disableDefaultUI: true,
						styles: [
							{
								"featureType": "landscape",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "poi",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "road",
								"elementType": "labels",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "transit",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							}
						]
					}

					var map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
					for (let item of this.getItem.event.Places) {
						var marker: google.maps.Marker = new google.maps.Marker({
							map: map,
							position: { lat: Number(item.latitude), lng: Number(item.longitude) },
						});
					}
				}
		}, error => {
			console.log(error);
		});

	}

	goBack() {
		this.navCtrl.back();
	}

	goToCreatePlan() {
		console.log(this.getItem);
		let navParam: NavigationExtras = {
			queryParams: {
				flag: 'city',
				city: JSON.stringify(this.getItem)
			}
		}
		this.router.navigate(['plan-create'], navParam);
	}

	goToSite() {
		if (get(this.getItem, 'event.website', '') !== '') {
			this.iab.create(get(this.getItem, 'event.website', ''), '_system');
		}
	}

	AddToCalender(item) {
		console.log(item);
		let title = '';
		let address = '';
		let description = '';
		if (this._globalServ.language == 'en') {
			title = item.name_EN;
			description = item.description_EN;
			if (item.Places) {
				address = item.Places.name_EN;
			}
		} else if (this._globalServ.language == 'es') {
			title = item.name_ES;
			description = item.description_ES;
			if (item.Places) {
				address = item.Places.name_ES;
			}
		}

		if (item.valid_until) {
			this.calendar.createEvent(title, address, description, new Date(item.valid_until), new Date(item.valid_until)).then(
				(msg) => {
					this.AddedToCalender = true;
					console.log(msg);
				},
				(err) => { console.log(err); }
				);
		}
	}
	async presentModal() {
		const modal = await this.modalController.create({
			component: ImageModalPage,
			cssClass: 'imageModal',
			backdropDismiss: true,
			componentProps: {
				image: this.url + '/get-image/' + this.getItem.event.image
			}
		});
		return await modal.present();
	}

	readMore(){
		this.read_flag = true;
	}

	checkHours(itemOpening2){
		var str1 = itemOpening2;
		var str2 = 'undefined';
		var res = str1.indexOf(str2);
				
		if(res != -1 || str1 == '' || str1 == null || !str1 ){
			return false;
		}
		return true;
	}

	mailShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaEmail("Hi! Download Vive IE app and join my agenda "+this.getItem.event.name_EN+" "+res.url, "Vive IE app", []).then(()=>{
				this.ga.trackEvent("City Agenda - "+this.getItem.event.name_EN, "Shared via Email");	
        	}).catch(()=>{
            	//this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});	
	}

	facebookShare(){
			this.branch.generateShortURL().then((res)=>{
				this.socialShare.shareViaFacebookWithPasteMessageHint("",null,res.url,"Hi! Download Vive IE app and join my agenda "+this.getItem.event.name_EN).then(()=>{
					this.ga.trackEvent("City Agenda - "+this.getItem.event.name_EN, "Shared via Facebook");
				}).catch(()=>{
					//this.showToast('App not found.');
				});
			}).catch((err)=>{
				console.log(err);
			});			
	}
	instagramShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaInstagram("Hi! Download Vive IE app and join my agenda "+this.getItem.event.name_EN+" "+res.url, null).then(()=>{
				this.ga.trackEvent("City Agenda - "+this.getItem.event.name_EN, "Shared via Instagram");
        	}).catch(()=>{
           		// this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});        
	}
    whatsappShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaWhatsApp("Hi! Download Vive IE app and join my agenda "+this.getItem.event.name_EN+" ",null,res.url).then(()=>{
				this.ga.trackEvent("City Agenda - "+this.getItem.event.name_EN, "Shared via Whatsapp");
			}).catch(()=>{
			   // this.showToast('App not found.');
			});
		}).catch((err)=>{
			console.log(err);
		});          
    }
    twitterShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaTwitter("Hi! Download Vive IE app and join my agenda "+this.getItem.event.name_EN+" ",null,res.url).then(()=>{
				this.ga.trackEvent("City Agenda - "+this.getItem.event.name_EN, "Shared via Twitter");
        	}).catch(()=>{
         		//   this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});        
	}
	async showToast(msg){
        const toast = await this.toastCtrl.create({
            message: msg,
            duration: 2000
        })
        await toast.present();
    }
}
