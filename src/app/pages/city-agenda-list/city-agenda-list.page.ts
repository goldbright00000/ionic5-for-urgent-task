import { Component, OnInit, ViewChild } from '@angular/core';
import { CityAgendaService } from '../../services/city-agenda.service';
import { GlobalService } from '../../services/global.service';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { NavController, IonRefresher } from "@ionic/angular";

@Component({
  selector: 'app-city-agenda-list',
  templateUrl: './city-agenda-list.page.html',
  styleUrls: ['./city-agenda-list.page.scss'],
})
export class CityAgendaListPage implements OnInit {

  getList: any;
  pageTitleName: any = false;
  @ViewChild(IonRefresher)
  refresher: IonRefresher;
  constructor(
    public _cityAgendaServ: CityAgendaService,
    public _globalServ: GlobalService,
    private router: Router,

    private navCtrl: NavController,
    private actRoute: ActivatedRoute,
  ) {
    this.actRoute.params.subscribe(val => {
      this._cityAgendaServ.getDataCityAgendaList();
    });
  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
  }

  ionViewWillLeave() {
    this.refresher.disabled = true;
  }
  ngOnInit() {
  }
  ionViewDidLeave() {
    this._globalServ.currentPreferences = undefined;
  }
  logScrolling(e) {
    if (e.detail.scrollTop > 50) {
      this.pageTitleName = true;
    } else {
      this.pageTitleName = false;
    }
  }

  openFilter() {

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'city_guide': 2 })
      }
    };
    this.navCtrl.navigateForward(['filters'], navigationExtras);
  }
  doRefresh(event) {
  
    console.log('Begin async operation');
    this._cityAgendaServ.getDataCityAgendaList();

    setTimeout(() => {
      console.log('Async operation has ended');
      event.target.complete();
    }, 2000);
  }
}
