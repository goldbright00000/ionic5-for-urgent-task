import { Component, OnInit, ViewChild, Input } from '@angular/core';
import { Location } from '@angular/common';
import { GlobalService } from '../../services/global.service';
import { CityGuideService } from '../../services/city-guide.service';
import { } from 'googlemaps';
import { ActivatedRoute } from '@angular/router';
import { environment } from '../../../environments/environment';
import { Router, NavigationExtras } from '@angular/router';
import { get } from 'lodash';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
import { AlertController, ModalController, NavController, ToastController } from '@ionic/angular';
import { CallNumber } from '@ionic-native/call-number/ngx';
import { TruncateModule } from '@yellowspot/ng-truncate';
import { ImageModalPage } from '../image-modal/image-modal.page';
import {SocialSharing} from "@ionic-native/social-sharing/ngx";
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';
import { BranchIOService } from 'src/app/services/branch-io.service';

@Component({
	selector: 'app-city-guide-item',
	templateUrl: './city-guide-item.page.html',
	styleUrls: ['./city-guide-item.page.scss'],
})
export class CityGuideItemPage implements OnInit {

	latitudeInit: any;
	longitudeInit: any;
	zoom: any = 14;
	getItem: any;
	id: any;
	url: any = environment.apiUrl;
	descLength:any;

	@ViewChild("map") mapElement;
	@Input() text: string;
	@Input() limit: number = 250;
	truncating = true;
	openingtimeArray: any;
	openingDatetime: any = [];

	constructor(
		private router: Router,
		public location: Location,
		public _globalServ: GlobalService,
		private _cityGuideServ: CityGuideService,
		private actRoute: ActivatedRoute,
		private iab: InAppBrowser,
		public alertController: AlertController,
		private callNumber: CallNumber,
		 private toastCtrl: ToastController,
		  private socialShare: SocialSharing,
		public modalController: ModalController,
		public navCtrl: NavController,
		public ga: GoogleAnalytics,
		public branch: BranchIOService,
		) {
		if (this.actRoute.snapshot.queryParams.data != null) {
			let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
			this.id = getParams.id;
		}
	}

	ngOnInit() {
	}

	async callWithNumber(phoneNumber) {
		const alert = await this.alertController.create({
			subHeader: phoneNumber,
			buttons: [
			{
				text: 'Cancel',
				role: 'cancel',
				cssClass: 'secondary',
				handler: (blah) => {
					console.log('Confirm Cancel: blah');
				}
			}, {
				text: 'Call',
				handler: () => {
					console.log('Confirm Okay');
					this.callNumber.callNumber(phoneNumber, true)
					.then(res => console.log('Launched dialer!', res))
					.catch(err => console.log('Error launching dialer', err));
				}
			}
			]
		});

		await alert.present();
	}

	ionViewDidEnter() {
		this._cityGuideServ.getItem(this.id)
			.subscribe(data => {
				this.getItem = data;
				this.ga.trackEvent("City Guide - "+this.getItem.article.name_EN, "Viewed");
				let openingDatetime = [];
				let openingtimeArray;
				if (this._globalServ.language == 'en') {
					if(this.getItem.article.Places.length > 0) {
						openingtimeArray = this.getItem.article.Places[0].hours_EN.split('\n');
					}
				}
				else {
					if(this.getItem.article.Places.length > 0) {
						openingtimeArray = this.getItem.article.Places[0].hours_ES.split('\n');
					}

				}
				if(openingtimeArray) {
					for (var i = 0; i < openingtimeArray.length; ++i) {
						if (openingtimeArray[i] != '')
							openingDatetime.push(openingtimeArray[i].split(': ')[0], openingtimeArray[i].split(': ')[1]);
					}
				}
				this.getItem.article.hours = openingDatetime;

				console.log(this.getItem)
				for (let item of this.getItem.article.Places) {
					this.latitudeInit = Number(item.latitude);
					this.longitudeInit = Number(item.longitude);
					console.log('latitude: ', this.latitudeInit);
					console.log('longitude: ', this.longitudeInit);
					break;
				}
				//set map
				if(this.latitudeInit && this.longitudeInit) {
					let coords = new google.maps.LatLng(this.latitudeInit, this.longitudeInit);
					let mapOptions: google.maps.MapOptions = {
						center: coords,
						zoom: this.zoom,
						maxZoom: 17,
						minZoom: 3,
						mapTypeId: google.maps.MapTypeId.ROADMAP,
						disableDefaultUI: true,
						styles: [
							{
								"featureType": "landscape",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "poi",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "road",
								"elementType": "labels",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							},
							{
								"featureType": "transit",
								"stylers": [
									{
										"visibility": "off"
									}
								]
							}
						]
					}

					var map = new google.maps.Map(this.mapElement.nativeElement, mapOptions);
					for (let item of this.getItem.article.Places) {
						console.log('latitude 2: ', item.latitude);
						console.log('longitude 2: ', item.longitude);
						var marker: google.maps.Marker = new google.maps.Marker({
							map: map,
							position: { lat: Number(item.latitude), lng: Number(item.longitude) },
						});
					}
				}
		}, error => {
			console.log(error);
		});
	}

	goBack() {
		this.navCtrl.back();
	}

	goToCreatePlan(Item) {

		let navigationExtras: NavigationExtras = {
			state: {
				item: Item.article,


			}
		};
		this.router.navigate(['plan-create'], navigationExtras);
	}

	goToSite(website) {
		console.log('link: ', get(website, 'article.website', ''))
		if (get(website, 'article.website', '') !== '') {
			this.iab.create(get(website, 'article.website', ''), '_system');
		}
	}
	async presentModal() {
		const modal = await this.modalController.create({
			component: ImageModalPage,
			cssClass: 'imageModal',
			backdropDismiss: true,
			componentProps: {
				image: this.url + '/get-image/' + this.getItem.article.image
			}
		});
		return await modal.present();
	}
	getdate(item) {

		this.openingtimeArray = item.hours_EN.split('\n');
		for (var i = 0; i < this.openingtimeArray.length; ++i) {
			if (this.openingtimeArray[i] != '')
				this.openingDatetime.push(this.openingtimeArray[i].split(': ')[0], this.openingtimeArray[i].split(': ')[1] + '<br><br>');
		}
		console.log(this.openingDatetime)
	}

	mailShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaEmail("Hi! Download Vive IE app and join my guide "+this.getItem.article.name_EN+" "+res.url, "Vive IE app", []).then(()=>{
				this.ga.trackEvent("City Guide - "+this.getItem.article.name_EN, "Shared via Email");		
        	}).catch(()=>{
            	//this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});			
	}

	facebookShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaFacebookWithPasteMessageHint("",null,res.url,"Hi! Download Vive IE app and join my guide "+this.getItem.article.name_EN).then(()=>{
				this.ga.trackEvent("City Guide - "+this.getItem.article.name_EN, "Shared via Facebook");
        	}).catch(()=>{
            	//this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});        
	}
	instagramShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaInstagram("Hi! Download Vive IE app and join my guide "+this.getItem.article.name_EN+" "+res.url, null).then(()=>{
				this.ga.trackEvent("City Guide - "+this.getItem.article.name_EN, "Shared via Instagram");
        	}).catch(()=>{
           		// this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});        
	}
    whatsappShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaWhatsApp("Hi! Download Vive IE app and join my guide "+this.getItem.article.name_EN+" ",null,res.url).then(()=>{
				this.ga.trackEvent("City Guide - "+this.getItem.article.name_EN, "Shared via Whatsapp");
			}).catch(()=>{
			   // this.showToast('App not found.');
			});
		}).catch((err)=>{
			console.log(err);
		});        
    }
    twitterShare(){
		this.branch.generateShortURL().then((res)=>{
			this.socialShare.shareViaTwitter("Hi! Download Vive IE app and join my guide "+this.getItem.article.name_EN+" ",null,res.url).then(()=>{
				this.ga.trackEvent("City Guide - "+this.getItem.article.name_EN, "Shared via Twitter");
        	}).catch(()=>{
         		//   this.showToast('App not found.');
        	});
		}).catch((err)=>{
			console.log(err);
		});         
	}
	async showToast(msg){
        const toast = await this.toastCtrl.create({
            message: msg,
            duration: 2000
        })
        await toast.present();
    }
}
