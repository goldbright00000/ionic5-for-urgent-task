import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';

import { IonicModule } from '@ionic/angular';

import { EditPlanPage } from './edit-plan.page';
import { ComponentModule } from '../../components/component.module';
import { AutocompletePageModule } from '../autocomplete/autocomplete.module';

import { Ionic4DatepickerModule } from
    '@logisticinfotech/ionic4-datepicker';

const routes: Routes = [
  {
    path: '',
    component: EditPlanPage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    AutocompletePageModule,
    RouterModule.forChild(routes),
    Ionic4DatepickerModule
  ],
  declarations: [EditPlanPage]
})
export class EditPlanPageModule {}
