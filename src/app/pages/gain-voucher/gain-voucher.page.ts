import { Component, OnInit } from '@angular/core';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { UserService } from 'src/app/services/user.service';
import { GlobalService } from 'src/app/services/global.service';
import { Platform } from '@ionic/angular';
import { Storage } from '@ionic/storage';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
	selector: 'app-gain-voucher',
	templateUrl: './gain-voucher.page.html',
	styleUrls: ['./gain-voucher.page.scss'],
})
export class GainVoucherPage implements OnInit {
	isVoucherSent: any = false;
	random: string;
	refer: any;
	hide: boolean;
	appURLAndroid: string;
	voucher: any = '';
	appURLiOS: any;

	constructor(private socialSharing: SocialSharing,
		public _globalServ: GlobalService,
		public _userServ: UserService,
		private storage: Storage,
		private platform: Platform,
		private ga: GoogleAnalytics,
		) {
		this._userServ.getCode(this._globalServ.idUser).subscribe((data: any) => {
			this.refer = data.referral_status[0].referral_code;
			console.log(this.refer)
			if (data.referral_status[0].referral_count >= 4) {
				this.isVoucherSent = true;
			}
			this.storage.get('offerApplied').then(res => {
				if (res) {
					this.voucher = res;
				} else {


					this._userServ.applyReward(this._globalServ.idUser).subscribe((data: any) => {
						console.log(data)
						this.voucher = data.voucher;

						this.storage.set('offerApplied', data.voucher);
					})


				}
			})


		})
		this._userServ.appInfo().subscribe((res: any) => {


			this.appURLiOS = res.app_info[1].url

			this.appURLAndroid = res.app_info[0].url
		})


	}

	ngOnInit() {
	}


	share(to) {
		let msg = "Hi! Join me and download VIVE IE app available in \n\nAndroid - " + this.appURLAndroid + "\n\niOS - " + this.appURLiOS
		if (to == "whatsapp") {
			this.socialSharing.shareViaWhatsApp(msg + '\n\nPlease use this promo code ' + this.refer, null, null).then((res) => {
				// Success!
				this.ga.trackEvent("Voucher", "Shared via Whatsapp");
				console.log(res)
			}).catch((err) => {
				// Error!
				console.log(err)
			});
		}
		if (to == "messanger") {
			this.socialSharing.shareVia("com.facebook.orca", msg + '\n\nPlease use this promo code ' + this.refer, null, null, null).then((res) => {
				// Success!
				this.ga.trackEvent("Voucher", "Shared via Messanger");
				console.log(res)
			}).catch((err) => {
				// Error!
				console.log(err)
			});
		}
		if (to == "any") {
			this.socialSharing.share(msg + '\n\nPlease use this promo code ' + this.refer, null, null, null).then((res) => {
				// Success!
				this.ga.trackEvent("Voucher", "Shared");
				console.log(res)
			}).catch((err) => {
				// Error!
				console.log(err)
			});
		}
		if (to == "sms") {
			this.socialSharing.shareViaSMS(msg + ' \n\nPlease use this promo code ' + this.refer, null).then((res) => {
				// Success!
				this.ga.trackEvent("Voucher", "Shared via SMS");
				console.log(res)
			}).catch((err) => {
				// Error!
				console.log(err)
			});
		}

	}
}
