import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupAllPage } from './group-all.page';

describe('GroupAllPage', () => {
  let component: GroupAllPage;
  let fixture: ComponentFixture<GroupAllPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupAllPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupAllPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
