import { Component, OnInit } from "@angular/core";
import { GroupService } from "src/app/services/group.service";
import * as moment from "moment";
import { Router, NavigationExtras } from "@angular/router";
import { ActivatedRoute } from "@angular/router";

@Component({
  selector: "app-group-calendar",
  templateUrl: "./group-calendar.page.html",
  styleUrls: ["./group-calendar.page.scss"]
})
export class GroupCalendarPage implements OnInit {
  monthNames: any;
  weekdays = ["S", "M", "T", "W", "T", "F", "S"];
  date: Date = new Date();
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  selectedDay: any = -1;
  cachedSelectedDay = null;
  allGroups: any = [];
  id: any;
  name: any;
  email: any;
  constructor(
    private _groupServ: GroupService,
    private actRoute: ActivatedRoute,
    private router: Router
  ) {}

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getDaysOfMonth();
  }

  ngOnInit() {
    if (this.actRoute.snapshot.queryParams.data != null) {
      let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
      this.id = getParams.id;
      this.name = getParams.name;
      this.email = getParams.email;
      console.log(getParams);
      this.loadAllGroups();
    }
    this.monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
  }

  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    this.currentDate = new Date().getDate();

    this.selectedDay = {
      day: this.date.getDate()
    };

    var firstDayThisMonth = new Date(
      this.date.getFullYear(),
      this.date.getMonth(),
      1
    ).getDay();

    var prevNumOfDays = new Date(
      this.date.getFullYear(),
      this.date.getMonth(),
      0
    ).getDate();

    for (
      var i = prevNumOfDays - (firstDayThisMonth - 0);
      i < prevNumOfDays;
      i++
    ) {
      this.daysInLastMonth.push(i);
    }

    var thisNumOfDays = new Date(
      this.date.getFullYear(),
      this.date.getMonth() + 1,
      0
    ).getDate();

    for (i = 0; i < thisNumOfDays; i++) {
      var obj = {
        day: i + 1,
        groups: this.checkGroups(i + 1)
      };
      this.daysInThisMonth.push(obj);

      if (i + 1 == new Date().getDate()) {
        this.currentDate = obj;
      }
      console.log(this.daysInThisMonth);
    }
  }

  loadAllGroups() {
    this._groupServ.getGroupPlans(this.id, this.email).subscribe(
      data => {
        this.allGroups = data;
        this.getDaysOfMonth();
      },
      err => {
        console.log("get group err: ", err);
      }
    );
  }

  checkGroups(day?) {
    var day_number = (day < 10 ? '0' : '') + day;
    var month_number = ((this.date.getMonth() + 1) < 10 ? '0' : '') + (this.date.getMonth() + 1);

    let date1 = this.date.getFullYear() +
    "-" +
    month_number +
    "-" +
    day_number;
    console.log('Date 1: ', date1);
    var groups = [];
    this.allGroups.groupPlans.forEach(element => {
      if (moment(element.plan_date).format("MM/DD/YYYY") ==
        moment(date1).format("MM/DD/YYYY")) {
        groups.push(element);
      }
    });

    return groups;
  }

  async goToItemPlan(dataPlan) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ id: dataPlan.id })
      }
    };
    this.router.navigate(["group-plan-item"], navigationExtras);
  }
}
