import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GroupCreateTypePage } from './group-create-type.page';

describe('GroupCreateTypePage', () => {
  let component: GroupCreateTypePage;
  let fixture: ComponentFixture<GroupCreateTypePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GroupCreateTypePage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GroupCreateTypePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
