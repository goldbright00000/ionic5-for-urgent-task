import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import {
	ToastController, NavController
} from '@ionic/angular';

@Component({
	selector: 'app-group-create-type',
	templateUrl: './group-create-type.page.html',
	styleUrls: ['./group-create-type.page.scss'],
})
export class GroupCreateTypePage implements OnInit {

	private: any;
	public: any;
	constructor(

		public _globalServ: GlobalService,
		public navCtrl: NavController,
		private toastController: ToastController,
	) {

		this.public = this._globalServ.grouptype == 'public' && 'public';
		this.private = this._globalServ.grouptype == 'private' && 'private';
	}

	ngOnInit() {
	}


	gettype(text) {
		this._globalServ.grouptype = text;
		this.navCtrl.back();
	}

	// _keyUp() {
	// 	const pattern = /[a-zA-z\+\-\ ]/;

	// 	let inputChar = String.fromCharCode(this.price);
	// 	console.log(this.price);
	// 	if (this.price.match(/[a-zA-Z]+$/)) {
	// 		console.log("notvalid")
	// 		this.valid = false;
	// 	} else {
	// 		console.log("valid")
	// 		this.valid = true;
	// 	}
	// }
	async goBack() {
		this.navCtrl.back();
	}

}
