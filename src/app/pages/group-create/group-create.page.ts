import { Component, OnInit, ViewChild } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { GlobalService } from '../../services/global.service';
import { Storage } from '@ionic/storage';
import {
  ToastController, Platform, AlertController, IonContent, ActionSheetController,
  NavController
} from '@ionic/angular';
import { UserService } from 'src/app/services/user.service';
import { get } from 'lodash';
import { environment } from '../../../environments/environment';
import { GroupService } from 'src/app/services/group.service';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import { Keyboard } from '@ionic-native/keyboard/ngx';
import { PlatformLocation } from '@angular/common';
import { GurdGuard } from "../../service/gurd/gurd.guard";
import { Crop } from '@ionic-native/crop/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
  selector: 'app-group-create',
  templateUrl: './group-create.page.html',
  styleUrls: ['./group-create.page.scss'],
})
export class GroupCreatePage implements OnInit {

  chosenImage: any = null;
  title: any = '';
  description: any = '';
  groupCreateSettingsValue: any;

  groupTypeValue: any;
  groupAdminSettingValue: any;

  pageTitleName: any = false;
  category: any;
  pick: boolean;
  done: boolean = true;
  @ViewChild('scrollContent') scroll: IonContent;
  public url: any = environment.apiUrl;
  public imageUrl: any;
  public pathImage: any;
  public pathImageCropper: any;
  public pathImagePost: any;
  public formValid: boolean = false;

  confirmedExit: boolean = true;
  adminlist: any = [];

  constructor(
    private router: Router,
    public _globalServ: GlobalService,
    public storage: Storage,
    private camera: Camera,
    private sanitizer: DomSanitizer,
    private webview: WebView,
    private toastController: ToastController,
    private _userServ: UserService,
    public _groupServ: GroupService,
    private keyboard: Keyboard,
    private transfer: FileTransfer,
    private platform: Platform,
    private crop: Crop,
    private loc: PlatformLocation,
    private alertController: AlertController,
    public actionSheetController: ActionSheetController,
    private actRoute: ActivatedRoute,
    public navCtrl: NavController,
    public ga: GoogleAnalytics
  ) {

    this.storage.set('group-create-settings', 1);
    this.storage.set('group-create-admin', []);
    this.groupTypeValue = this._globalServ.grouptype == 'public' ? this._globalServ.language === "en" ? 'Public' : 'Público' : this._globalServ.language === "en" ? 'Private' : 'Privado';


    this.groupCreateSettingsValue = this._globalServ.language === "en" ? 'Members can create a plan' : "Los miembros pueden crear un plan";
    if (this.platform.is('android')) {
      window.addEventListener('keyboardWillShow', (event: any) => {
        console.log("keyboard show", event)
        document.getElementById('container').style.marginBottom = (event.keyboardHeight) + 'px';
        this.scroll.scrollToPoint(0, event.path[0].document.activeElement.offsetParent.offsetTop, 500)
      })

      window.addEventListener('keyboardDidHide', (event: any) => {
        console.log("keyboard show", event)
        document.getElementById('container').style.marginBottom = 'unset';
      })
    }
  }



  ngOnInit() {
  }

  logScrolling(e) {
    if (e.detail.scrollTop > 50) {
      this.pageTitleName = true;
    } else {
      this.pageTitleName = false;
    }
  }



  ionViewDidEnter() {
    this.groupTypeValue = this._globalServ.grouptype == 'public' ? this._globalServ.language === "en" ? 'Public' : 'Público' : this._globalServ.language === "en" ? 'Private' : 'Privado';

    this.validate();
    this.category = [];
    this.storage.get('group-create-category').then((val) => {
      this.pick = false;
      this._groupServ.getGroupCategories().subscribe((data: any) => {
        for (let i = 0; i < data.length; i++) {
          for (let j = 0; j < val.length; j++) {
            if (data[i].id == val[j]) {
              if (this._globalServ.language == 'en') {
                this.category.push(data[i]);
                this.pick = true;
              }
              else {
                this.pick = true;
                this.category.push(data[i]);
              }
            }

          }
        }
        this.validate();
      })
    });

    this.actRoute.queryParams.subscribe(data => {
      if (data && data.data) {
        console.log(JSON.parse(data.data));
        let item = JSON.parse(data.data)
        if (item.hasOwnProperty('item')) {
          console.log(item);
          this.title = item.item.group.title;
          this.description = item.item.group.description;
          this.chosenImage = item.item.group.image;
          this.pathImageCropper = item.item.group.image;
          this._globalServ.groupImagePath = this.chosenImage;
          this.category = item.item.groupCategories;
          this.storage.set('group-create-category', this.category);
          this.groupCreateSettingsValue = item.item.groupSettings.Groups_Setting.name;
          console.log("category", this.category);
        }
      }

    })

    console.log('this._globalServ.groupImagePath:  ', this._globalServ.groupImagePath);
    this.chosenImage = this._globalServ.groupImagePath ? this._globalServ.groupImagePath : null;
    this.getGroupSettings();
    this.getAdminSettings();
    localStorage.setItem('goBack', 'true');
  }

  async ionViewWillLeave() {
    //this._globalServ.invited = null;
  }



  createCategory() {
    localStorage.setItem('goBack', 'false');
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'group_create': true })
      }
    };
    // this.router.navigate(['preferences'], navigationExtras);
    // this.router.navigate(['preferences']);
    // this.router.navigate(['group-create-category'], navigationExtras);
    this.navCtrl.navigateForward(['group-create-category'], navigationExtras)
  }

  pickSettings() {
    localStorage.setItem('goBack', 'false');
    this.router.navigate(['group-create-settings']);
  }

  adminPeopel() {
    localStorage.setItem('goBack', 'false');
    this.router.navigate(['group-create-admin']);
  }

  invitePeople() {
    localStorage.setItem('goBack', 'false');
    this.router.navigate(['group-create-invite']);
  }

  setPlanType() {
    localStorage.setItem('goBack', 'false');
    this.router.navigate(['group-create-type']);
  }

  async showAlert(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }

  doCreateGroup(params) {
    console.log(params);
    localStorage.setItem('goBack', 'false');
    console.log('<<<  starting group creating....   >>>');
    let navigationExtras: NavigationExtras = {
      state: {
        title: this.title,
      }
    };
    this._globalServ.groupImagePost = this._globalServ.groupImagePost ? this._globalServ.groupImagePost : "";
    // this._groupServ.createGroup(params, this._globalServ.groupImagePost).subscribe(data => {
    //   this.done = true;
    //   //this.showAlert('Created a group successfully');
    //   console.log('<<<<created group successfully !!!>>>>');

    //   this.storage.set('group-create-category', []);
    //   this.storage.set('group-create-settings', -1); // null or -1
    //   this.storage.set('group-create-members', []);
    //   this._globalServ.groupImagePath = null;
    //   this._globalServ.groupImagePost = null;
    //   this.title = this.description = '';

    //   this.chosenImage = null;

    //   this.router.navigate(['group-create-success'], navigationExtras);
    // }, err => {
    //   console.log('err: ', err);
    //   this.done = false;
    //   //  this.showAlert(err.message);
    // })
    const fileTransfer: FileTransferObject = this.transfer.create();
    let filename = 'groupImage_' + Math.random() * 100000000000000000;
    let options: FileUploadOptions = {
      fileKey: 'image',
      fileName: filename + '.jpg',
      params: params,
    }

    fileTransfer.upload(this._globalServ.groupImagePost, environment.apiUrl + '/add-group', options)
      .then((data) => {
        //   this.showAlert('Created a group successfully');
        console.log('<<<<created group successfully !!!>>>>');
        this.ga.trackEvent("Group - " + this.title, "Created");
        this.storage.set('group-create-category', []);
        this.storage.set('group-create-settings', -1); // null or -1
        this.storage.set('group-create-members', []);
        this.storage.set('group-create-admin', []);
        this._globalServ.groupImagePath = null;
        this._globalServ.groupImagePost = null;
        this.title = this.description = '';

        this.chosenImage = null;

        this.router.navigate(['group-create-success'], navigationExtras);
      }, (err) => {
        // error
        console.log('error', err);
        this.done = false;
        // this.showAlert(err.message);
      });
  }

  async presentAlert() {
    const alert = await this.alertController.create({
      header: 'Discard Group ?',
      message: "Your group hasn't been created yet.If you discard now , your progress won't be saved.",
      buttons: [
        {
          text: 'Keep',
          cssClass: 'secondary',
          handler: (blah) => {
            console.log('KEEP');
          }
        }, {
          text: 'Discard',
          role: 'cancel',
          handler: () => {
            this.navCtrl.back();
            this.confirmedExit = true;
            this._globalServ.invited = null;
            this.storage.set('group-create-members', []);
            this.storage.set('group-create-admin', []);
            this.storage.set('group-create-category', []);
            console.log('Cancel');
          }
        }
      ]
    });
    await alert.present()
  }

  getGroupSettings() {
    this._groupServ.getGroupSettings().subscribe(data => {
      console.log('group settings data: ', data);
      let groupSettings = [];
      groupSettings = JSON.parse(JSON.stringify(data));
      this.storage.get('group-create-settings').then((groupSettingsVal) => {
        console.log('groupSettingsVal--- : ', groupSettingsVal);
        if (groupSettingsVal > 0) {
          this.groupCreateSettingsValue = groupSettings.filter(o => o.id === groupSettingsVal)[0].name;
          if (this.groupCreateSettingsValue === "Members can create a plan") {
            this.groupCreateSettingsValue = this._globalServ.language === "en" ? "Members can create a plan" : "Los miembros pueden crear un plan";
          }
          if (this.groupCreateSettingsValue === "Members cannot create a plan") {
            this.groupCreateSettingsValue = this._globalServ.language === "en" ? "Members cannot create a plan" : "Los miembros no pueden crear un plan";
          }
        } else {
          this.groupCreateSettingsValue = this._globalServ.language === "en" ? "Members can create a plan" : "Los miembros pueden crear un plan";
        }
      });
    }, err => {
      console.log('group settings err: ', err);
    })
  }

  getAdminSettings() {
    this._groupServ.getPeoplesForInvitation().subscribe(data => {

      let groupAdminSettings = [];
      groupAdminSettings = JSON.parse(JSON.stringify(data));

      this.storage.get('group-create-admin').then((groupAdminSettingsVal) => {
        this.adminlist = groupAdminSettingsVal;
        console.log('groupSettingsVal--- : ', groupAdminSettingsVal);
        if (groupAdminSettingsVal[0] > 0) {
          this.groupAdminSettingValue = groupAdminSettings.filter(o => o.id === groupAdminSettingsVal[0])[0].name;
          console.log(this.groupAdminSettingValue);
        }
      });
    }, err => {
      console.log('group settings err: ', err);
    })
  }

  selectFile(source) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: source
    }

    this.camera.getPicture(options).then((imageData) => {
      this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(imageData));
      this.pathImageCropper = imageData;
      this.pathImagePost = imageData;
      this._globalServ.groupImagePath = this.pathImage;
      this._globalServ.groupImagePost = this.pathImagePost;
      this.chosenImage = this.pathImage;
      this.cropFunc();

      console.log("Image path postt", this.pathImagePost);
    }, (err) => {
      console.log(err);
    });
  }
  cropFunc() {
    this.crop.crop(this.pathImageCropper, { quality: 75 })
      .then(
        newImage => {
          this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(newImage));
          this.pathImageCropper = newImage;
          this.pathImagePost = newImage;
          this._globalServ.groupImagePath = this.pathImage;
          this._globalServ.groupImagePost = this.pathImagePost;
          this.chosenImage = this.pathImage;
        },
        error => console.error('Error cropping image', error)
      );
  }
  async goToselectImage() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'plan': true })
      }
    };
    let titleLabel;
    let cancelLabel;
    let EditLabel;
    let ChangeLabel;
    let RemoveLabel;
    if (this._globalServ.language == 'en') {
      titleLabel = 'Photo';
      cancelLabel = 'Cancel';
      EditLabel = 'Edit Photo';
      ChangeLabel = 'Change Photo';
      RemoveLabel = 'Remove Photo';
    }
    if (this._globalServ.language == 'es') {
      titleLabel = '';
      cancelLabel = 'Cancelar';
      EditLabel = 'Editar foto';
      ChangeLabel = 'Cambiar Fotografía';
      RemoveLabel = 'Eliminar foto'
    }
    // const actionSheetForCancel = await this.actionSheetController.create({
    // 	cssClass: 'actionsheet2',
    // 	buttons: [{
    // 		text: cancelLabel,
    // 		handler: () => {
    // 			actionSheetForPlan_group.dismiss()
    // 		}
    // 	}
    // 	]
    // });


    const actionSheetForPlan_group = await this.actionSheetController.create({
      header: titleLabel,

      buttons: [
        {
          text: EditLabel,

          handler: () => {

            // this.router.navigate(['image-cropper'], navigationExtras);
            this.cropFunc();

          }
        },
        {
          text: ChangeLabel,
          handler: () => {
            this.selectFile(2);
            // this.router.navigate(['image-cropper'], navigationExtras);
          }
        },
        {
          text: RemoveLabel,
          handler: () => {
            this._globalServ.planImagePath = null;



          }
        },
        {
          text: cancelLabel,

          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    }).then(actionsheet => {
      actionsheet.present();
    });;



  }
  async goToselectImg() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'plan': true })
      }
    };
    let titleLabel;
    let cancelLabel;
    let EditLabel;
    let ChangeLabel;
    let RemoveLabel;
    if (this._globalServ.language == 'en') {
      titleLabel = 'Photo';
      cancelLabel = 'Cancel';
      EditLabel = 'Take Picture';
      ChangeLabel = 'Choose Photo';
    }
    if (this._globalServ.language == 'es') {
      titleLabel = '';
      cancelLabel = 'Cancelar';
      EditLabel = 'Tomar la foto';
      ChangeLabel = 'Escoge una foto';
    }
    const actionSheetForCancel = await this.actionSheetController.create({
      cssClass: 'actionsheet2',
      buttons: [{
        text: cancelLabel,
        handler: () => {
          actionSheetForPlan_group.dismiss()
        }
      }
      ]
    });


    const actionSheetForPlan_group = await this.actionSheetController.create({
      header: titleLabel,
      cssClass: 'actionSheetForPlan_group',
      buttons: [
        {
          text: EditLabel,

          handler: () => {
            actionSheetForCancel.dismiss()
            this.selectFile(1);

            // this.cropFunc();
            // this.router.navigate(['image-cropper'], navigationExtras);


          }
        },
        {
          text: ChangeLabel,
          handler: () => {
            actionSheetForCancel.dismiss()
            this.selectFile(2);
            // this.router.navigate(['image-cropper'], navigationExtras);
          }
        }
      ]
    });
    actionSheetForCancel.present();
    actionSheetForPlan_group.present();

    actionSheetForPlan_group.onDidDismiss().then(() => {
      actionSheetForCancel.dismiss();
    });
    actionSheetForCancel.onDidDismiss().then(() => {
      actionSheetForPlan_group.dismiss();
    });
    //	this.router.navigate(['image-cropper'], navigationExtras);
  }
  save() {

    let noticeMsg = '';
    let group_supercategory_ids = '';
    let group_settings_ids = '';
    let group_member_ids = '';
    let groupadmins = '';

    let email;
    let fk_user_id;

    this._userServ.getPublicProfile(this._globalServ.idUser).subscribe(data => {
      let profile = get(data, '[0]', {});
      email = get(profile, 'email', '');
      fk_user_id = this._globalServ.idUser;

      if (this._globalServ.groupImagePath) {
        this.storage.get('group-create-category').then((groupSuperCategoryVal) => {
          if (groupSuperCategoryVal.length <= 3 && groupSuperCategoryVal.length !== 0) {
            group_supercategory_ids = groupSuperCategoryVal.toString();

            this.storage.get('group-create-settings').then((groupSettingsVal) => {
              if (groupSettingsVal > 0) {
                group_settings_ids = groupSettingsVal.toString();

                this.storage.get('group-create-admin').then((groupadmin) => {
                  console.log(groupadmin)
                  if (groupadmin.length >= 0) {
                    groupadmins = groupadmin.toString();
                    this.storage.get('group-create-members').then((groupMemberVal) => {
                      group_member_ids = groupMemberVal.toString();
                      if (this.title !== '') {
                        if (this.description !== '') {
                          let params = {
                            fk_user_id,
                            groupadmins,
                            planType: this._globalServ.grouptype == 'public' ? 0 : 1,
                            title: this.title,
                            description: this.description,
                            group_settings_ids,
                            group_supercategory_ids,
                            group_member_ids,
                            email
                          }
                          this.done = false;
                          this.doCreateGroup(params);
                        } else {
                          this.showAlert('Please fill in the group description');
                        }
                      } else {
                        this.showAlert('Please fill in the group title');
                      }
                    });
                  }
                });
              } else {
                this.showAlert('Please choose group setting');
              }
            });
          } else {
            this.showAlert('It is limited to choose 3 categories or Please choose categories');
          }
        });
      } else {
        this.showAlert('Please upload group image');
      }

    }, err => {
      console.log('err public profile: ', err);
    });
  }

  hideKeyboard() {
    this.keyboard.hide();
  }

  // selectFile(source) {
  //     const options: CameraOptions = {
  //         quality: 100,
  //         destinationType: this.camera.DestinationType.FILE_URI,
  //         encodingType: this.camera.EncodingType.JPEG,
  //         mediaType: this.camera.MediaType.PICTURE,
  //         sourceType: source
  //     };
  //     this.camera.getPicture(options).then((imageData) => {
  //         this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(imageData));
  //         this.pathImageCropper = imageData;
  //         this.pathImagePost = imageData;
  //         this.cropFunc();
  //         console.log("Image path postt", this.pathImagePost);
  //     }, (err) => {
  //         console.log(err);
  //     });
  // }

  // cropFunc() {
  //     this.crop.crop(this.pathImageCropper, { quality: 75 })
  //         .then(
  //             newImage => {
  //                 this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(newImage));
  //                 this.pathImageCropper = newImage;
  //                 this.pathImagePost = newImage;
  //                 this.uploadProfile();
  //             },
  //             error => console.error('Error cropping image', error)
  //         );
  // }

  uploadProfile() {
    const fileTransfer: FileTransferObject = this.transfer.create();
    let filename = 'profileImage_' + Math.random() * 100000000000000000 + '.jpg';

    let options: FileUploadOptions = {
      fileKey: 'image',
      fileName: filename,
    };

    fileTransfer.upload(this.pathImagePost, environment.apiUrl + '/app-user-set-profile-image/' + this._globalServ.idUser, options)
      .then((data) => {
        this._globalServ.planImagePath = this.pathImage;
        this._globalServ.planImagePost = this.pathImagePost;
        console.log('success', data);
        this.router.navigate(['group-create'], {});
      }, (err) => {
        console.log('error', err);
        this.router.navigate(['group-create'], {});
      })
  }
  goBack() {
    if (this.confirmedExit == false) {
      this.presentAlert();
    } else {
      this.confirmedExit = true;
      this._globalServ.invited = null;
      this.storage.set('group-create-members', []);
      this.storage.set('group-create-category', []);
      this.storage.set('group-create-admin', []);
    }
  }


  validate() {
    this.confirmedExit = this.title != '' || this.description != '' ? false : true;
    this.formValid = this.title && this.category && this.category.length && this.description ? true : false;
    console.log("Valid: ", this.formValid);
  }
}
