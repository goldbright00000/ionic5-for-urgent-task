import { Component, Input } from '@angular/core';
import { NavParams, ModalController } from '@ionic/angular';

@Component({
	selector: 'image-modal',
	templateUrl: './image-modal.page.html',
	styleUrls: ['./image-modal.page.scss'],
})
export class ImageModalPage  {
	@Input() image: string;
	url: any;
	constructor(
		public navParams: NavParams,
		public modalController: ModalController) {
		this.url = this.navParams.get('image');
	}

	dismiss() {
		this.modalController.dismiss({
      'dismissed': true
    });
	}

}
