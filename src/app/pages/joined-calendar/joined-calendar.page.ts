import { Component, OnInit } from "@angular/core";
import { GroupService } from "src/app/services/group.service";
import * as moment from "moment";
import { GlobalService } from "src/app/services/global.service";
import { Router, NavigationExtras } from "@angular/router";
import { ToastController } from "@ionic/angular";
import { PlanService } from "src/app/services/plan.service";
import { environment } from "../../../environments/environment";

@Component({
  selector: "app-joined-calendar",
  templateUrl: "./joined-calendar.page.html",
  styleUrls: ["./joined-calendar.page.scss"]
})
export class JoinedCalendarPage implements OnInit {
  monthNames: any;
  weekdays = ["S", "M", "T", "W", "T", "F", "S"];
  date: Date = new Date();
  daysInThisMonth: any;
  daysInLastMonth: any;
  daysInNextMonth: any;
  currentMonth: any;
  currentYear: any;
  currentDate: any;
  selectedDay: any = -1;
  cachedSelectedDay = null;
  allGroups: any = [];
  allPlans: any = [];
  recommendedPlans: any = [];
  url: any = environment.apiUrl;
  constructor(
    private _groupServ: GroupService,
    public _globalServ: GlobalService,
    public _planServ: PlanService,
    private router: Router,
    private toastController: ToastController
  ) {}

  ngOnInit() {
    this.monthNames = [
      "January",
      "February",
      "March",
      "April",
      "May",
      "June",
      "July",
      "August",
      "September",
      "October",
      "November",
      "December"
    ];
    this.loadPlans();
    console.log(this._globalServ);
  }

  goToLastMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth(), 0);
    this.getDaysOfMonth();
  }

  goToNextMonth() {
    this.date = new Date(this.date.getFullYear(), this.date.getMonth() + 2, 0);
    this.getDaysOfMonth();
  }

  getDaysOfMonth() {
    this.daysInThisMonth = new Array();
    this.daysInLastMonth = new Array();
    this.daysInNextMonth = new Array();
    this.currentMonth = this.monthNames[this.date.getMonth()];
    this.currentYear = this.date.getFullYear();
    this.currentDate = new Date().getDate();

    this.selectedDay = {
      day: this.date.getDate()
    };

    var firstDayThisMonth = new Date(
      this.date.getFullYear(),
      this.date.getMonth(),
      1
    ).getDay();

    var prevNumOfDays = new Date(
      this.date.getFullYear(),
      this.date.getMonth(),
      0
    ).getDate();

    for (
      var i = prevNumOfDays - (firstDayThisMonth - 0);
      i < prevNumOfDays;
      i++
    ) {
      this.daysInLastMonth.push(i);
    }

    var thisNumOfDays = new Date(
      this.date.getFullYear(),
      this.date.getMonth() + 1,
      0
    ).getDate();

    for (i = 0; i < thisNumOfDays; i++) {
      var obj = this.checkGroups(i + 1);
      this.daysInThisMonth.push(obj);

      if (i + 1 == new Date().getDate()) {
        this.currentDate = obj;
      }
    }
    console.log(this.daysInThisMonth);
  }

  loadPlans() {
    console.log(this._globalServ);
    this._planServ
      .getParticipations(this._globalServ.idUser)
      .subscribe(data => {
        console.log(data);
        this.allPlans = data;
        for (let item of this.allPlans) {
          item.image = this.url + "/get-ie-image-plan/" + item.image;
          item.join = true;
          item.type = "student_plan";
        }

        this._groupServ.joinedGroupPlans(this._globalServ.idUser).subscribe(
          data => {
            console.log(data);
            this.allGroups = data;
            this.getDaysOfMonth();
          },
          err => {
            console.log("joined gropu plan err: ", err);
          }
        );
      });
  }

  checkGroups(day?) {
    var day_number = (day < 10 ? '0' : '') + day;
    var month_number = ((this.date.getMonth() + 1) < 10 ? '0' : '') + (this.date.getMonth() + 1);

    let date1 = this.date.getFullYear() +
    "-" +
    month_number +
    "-" +
    day_number;
    var groups = [];
    var studentPlans = [];
    var recommendedPlans = [];
    this.allGroups.forEach(element => {
      if (moment(element.plan_date).format("MM/DD/YYYY") ==
          moment(date1).format("MM/DD/YYYY")) {
        groups.push(element);
      }
    });

    this.allPlans.forEach(element => {
      if (moment(element.plan_date).format("MM/DD/YYYY") ==
        moment(date1).format("MM/DD/YYYY")) {
        if (element.highlight == 1) {
          recommendedPlans.push(element);
        } else {
          studentPlans.push(element);
        }
      }
    });

    return {
      day: day,
      groups: groups,
      studentPlans: studentPlans,
      recommendedPlans: recommendedPlans
    };
  }

  async goToItemPlan(dataPlan, type) {
    console.log(dataPlan);
    if (type !== "group_plan") {
      console.log(dataPlan);
      //change yagnesh
      if (dataPlan.private && !dataPlan.can_access_private_plan) {
        const toast = await this.toastController.create({
          message: "You do not have access to this private plan",
          duration: 2000
        });
        toast.present();
      } else {
        let navigationExtras: NavigationExtras = {
          queryParams: {
            data: JSON.stringify({ id: dataPlan.id, type: dataPlan.type })
          }
        };
        this.router.navigate(["plan-item"], navigationExtras);
      }
    } else {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          data: JSON.stringify({ id: dataPlan.id })
        }
      };
      this.router.navigate(["group-plan-item"], navigationExtras);
    }
  }
}
