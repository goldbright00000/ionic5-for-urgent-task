import { Component, OnInit } from '@angular/core';
import { GroupService } from '../../services/group.service';
import { GlobalService } from '../../services/global.service';
import { environment } from '../../../environments/environment';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-joined-group-plans',
  templateUrl: './joined-group-plans.page.html',
  styleUrls: ['./joined-group-plans.page.scss'],
})
export class JoinedGroupPlansPage implements OnInit {

  joinedGroupPlans: any;
  url: any = environment.apiUrl;

  constructor(
    public router: Router,
    public _groupServ: GroupService,
    public _globalServ: GlobalService,
    ) {
    this._groupServ.joinedGroupPlans(this._globalServ.idUser).subscribe(data => {
      this.joinedGroupPlans = data;
      console.log('joined group plans scc: ', this.joinedGroupPlans);
    }, err => {
      console.log('joined gropu plan err: ', err);
    })
  }

  goToGroups() {
    this.router.navigate(['groups']);
  }

  ngOnInit() {
  }

  goToPlan(){
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({})
      }, replaceUrl: true
    };
    this.router.navigate(['tabs/plan-list'], navigationExtras)
  }

  goToGroupItem(group) {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'id': group.id })
      },
    };
    this.router.navigate(['group-plan-item'], navigationExtras);
  }

  join(id,index){
    console.log("REMOVE", id);
      this._groupServ.leaveGroupPlan(this._globalServ.idUser, null).subscribe(data => {
        console.log(data);
        this.joinedGroupPlans.splice(index,1);

        console.log(">>>",this.joinedGroupPlans);
      }, error => {
        console.log(error);
      })
  }

  getDate(plandate) {
		const monthNames = ["Jan", "Feb", "Mar", "Apr", "May", "Jun",
		"Jul", "Aug", "Sep", "Oct", "Nov", "Dec"
		];
		let dateCommon = new Date(plandate).toDateString();

		let day = dateCommon.slice(0, dateCommon.indexOf(' '));
		let date = new Date(plandate).getDate();
		let month = monthNames[new Date(plandate).getMonth()];
		let year = new Date(plandate).getFullYear();
		let hours = new Date(plandate).getUTCHours();
		let minutes: any = new Date(plandate).getUTCMinutes();
		if (minutes < 10)
			minutes = '0' + minutes;
		else
			minutes = '' + minutes;
		return day + ', ' + date + ' ' + month + ' ' + year + ', ' + hours + ':' + minutes
	}

}
