import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import { NotificationService } from '../../services/notification.service';
import { UserService } from '../../services/user.service';
import { get } from 'lodash';
import { Router, NavigationExtras } from '@angular/router';

@Component({
  selector: 'app-notifications',
  templateUrl: './notifications.page.html',
  styleUrls: ['./notifications.page.scss'],
})
export class NotificationsPage implements OnInit {

  profile: any = {};
  notifications: any = [];
  isTodayNotification: any = false;
  onLoadClass: any;
  month_notification;
  week_notification;
  selectedType: any;

  constructor(
    public _globalServ: GlobalService,
    public _notificationServ: NotificationService,
    public _userServ: UserService,
    private router: Router,
  ) {

    this.selectedType = 'all';
  }

  ngOnInit() {
    this.getProfile();
  }

  segmentChanged(eve) {
    this.selectedType = eve.detail.value;
    this.getNotifications();
  }

  getProfile() {
    this._userServ.getPublicProfile(this._globalServ.idUser).subscribe(data => {
      this.profile = get(data, '[0]', {});
      this.getNotifications();
      console.log('this.profile: ', this.profile);
    }, err => {
      console.log('profile err:', err);
    });
  }

  getDuration(createdDateStr) {
    let now = new Date().getTime();
    let created = new Date(createdDateStr).getTime();

    if (now > created) {
      let days = Math.round((now - created) / 3600 / 1000 / 24);
      let origDays = (now - created) / 3600 / 1000 / 24;
      if (days === 0) {
        return Math.round((now - created) / 3600 / 1000) + ' hours ago';
      } else if (days > 0 && origDays <= 6 && days <= 6) {
        if (days === 1) {
          return 'yesterday';
        } else {
          let days = ['Sunday', 'Monday', 'Tuesday', 'Wednesday', 'Thursday', 'Friday', 'Saturday'];
          if (this._globalServ.language == 'es') {
            days = ['Domingo', 'Lunes', 'Martes', 'Mi�rcoles', 'Jueves', 'Viernes', 'S�bado'];
          }
          return days[new Date(createdDateStr).getDay()];
        }
      } else {
        var weekVar = Math.round(((now - created) / 3600 / 1000 / 24 / 7));
        if (weekVar == 1) {
          return weekVar + ' week ago';
        } else {
          return weekVar + ' weeks ago';
        }
        // return Math.round(((now-created) / 3600 / 1000 / 24 / 7)) + ' week ago';
      }
    } else {
      return '';
    }
  }

  readNotification(notification) {
    let param = {
      userId: this._globalServ.idUser,
      email: this.profile.email
    };
    this._notificationServ.readNotifications(notification.id, param).subscribe(data => {
      console.log('read success: ', data);
      this.getNotifications();
    }, err => {
      console.log('read err: ', err);
    })
  }


  acceptNotification(notification) {
    let param = {
      userId: this._globalServ.idUser,
    };
    this._notificationServ.acceptNotifications(notification.id, param).subscribe(data => {
      console.log('read success: ', data);
      this.getNotifications();
    }, err => {
      console.log('read err: ', err);
    })
  }

  declineNotification(notification) {
    let param = {
      userId: this._globalServ.idUser,
    };
    this._notificationServ.declineNotifications(notification.id, param).subscribe(data => {
      console.log('read success: ', data);
      this.getNotifications();
    }, err => {
      console.log('read err: ', err);
    })
  }

  getNotifications() {
    console.log(" getNotifications() ");
    this._notificationServ.getNotifications(this.profile.email).subscribe(data => {
      console.log('notification data: ', data);
      this.notifications = get(data, 'notifications');

      // this.notifications = [];

      if (this.selectedType == 'all') {
        this.notifications = this.notifications.filter((data) => {
          return data.type != 'VS_GROUP_REQUESTS'
        });
      }

      else {
        this.notifications = this.notifications.filter((data) => {
          return data.type == 'VS_GROUP_REQUESTS'
        });
      }


      for (var i = 0; i < this.notifications.length; ++i) {
        if (this.notifications[i].date_group == 'TODAY') {
          this.isTodayNotification = true;
          // return;
        }
      }

      for (var i = 0; i < this.notifications.length; ++i) {
        if (this.notifications[i].date_group == 'THIS WEEK') {
          this.week_notification = true;
        }
        if (this.notifications[i].date_group == 'THIS MONTH') {
          this.month_notification = true;
        }
      }

    }, err => {
      console.log('notification err: ', err);
    });
  }

  goThroughNotification(notiData) {
    if (notiData.type == 'VS_GROUPS' || notiData.type == 'VS_GROUP_MEMBERS') {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          data: JSON.stringify({ 'id': notiData.object_id })
        },
      };
      this.router.navigate(['group-detail'], navigationExtras)
    }

    if (notiData.type == 'IE_DISCOUNTS') {
      var discData;
      this._notificationServ.getOfferAnddiscount(notiData.object_id).subscribe(data => {
        discData = data;
        let navigationExtras: NavigationExtras = {
          queryParams: {
            data: JSON.stringify({ 'item': discData.discount })
          }
        };
        this.router.navigate(['/offers-discoutns-item'], navigationExtras);
      });
    }

    if (notiData.type == 'VS_GROUP_PLAN_PARTICIPANTS') {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          data: JSON.stringify({ 'id': notiData.object_id, 'type': 'group_plan' })
        }
      };
      this.router.navigate(['/group-plan-item'], navigationExtras);
    }
    if (notiData.type == 'IE_PARTICIPANTS') {
      let navigationExtras: NavigationExtras = {
        queryParams: {
          data: JSON.stringify({ 'id': notiData.object_id, 'type': 'student_plan' })
        }
      };
      this.router.navigate(['/plan-item'], navigationExtras);
    }
  }

}
