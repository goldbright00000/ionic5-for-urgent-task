import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { GoogleAnalytics } from '@ionic-native/google-analytics/ngx';

@Component({
  selector: 'app-offers-discounts-code',
  templateUrl: './offers-discounts-code.page.html',
  styleUrls: ['./offers-discounts-code.page.scss'],
})
export class OffersDiscountsCodePage implements OnInit {

  code: any;
  item_info;
  constructor(
    private actRoute: ActivatedRoute,
    private ga: GoogleAnalytics,
    ) {

     this.actRoute.queryParams.subscribe(data => {

      console.log("MMM",data);
      let mydata = JSON.parse(data.data);
      console.log(mydata.code);
      this.item_info = mydata.code;
      this.ga.trackEvent("Discount Code - "+this.item_info.discountCode, "Viewed");
     })

    if (this.actRoute.snapshot.queryParams.data != null) {
      let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
      this.code = getParams.code;
    }
  }

  ngOnInit() {
  }

}
