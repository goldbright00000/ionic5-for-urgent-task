import { Component, OnInit, ViewChild } from '@angular/core';
import { OfferDiscountService } from '../../services/offer-discount.service';
import { GlobalService } from '../../services/global.service';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { NavController, IonRefresher } from "@ionic/angular";

@Component({
  selector: 'app-offers-discounts-list',
  templateUrl: './offers-discounts-list.page.html',
  styleUrls: ['./offers-discounts-list.page.scss'],
})
export class OffersDiscountsListPage implements OnInit {
  pageTitleName: any = false;
  @ViewChild(IonRefresher)
  refresher: IonRefresher;
  constructor(
    private _offerDiscountServ: OfferDiscountService,
    public _globalServ: GlobalService,
    private router: Router,

    private navCtrl: NavController,
    private actRoute: ActivatedRoute,
  ) {
    this.actRoute.params.subscribe(val => {
      this._offerDiscountServ.getDataOfferDiscountList();
    });
  }

  ngOnInit() {
  }
  ionViewDidEnter() {
    this.refresher.disabled = false;
  }

  ionViewWillLeave() {
    this.refresher.disabled = true;
  }
  logScrolling(e) {
    if (e.detail.scrollTop > 50) {
      this.pageTitleName = true;
    } else {
      this.pageTitleName = false;
    }
  }

  openFilter() {

    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'city_guide': 3 })
      }
    };
    this.navCtrl.navigateForward(['filters'], navigationExtras);
  }
  async doRefresh(event) {
 
    console.log('Begin async operation');
    this._offerDiscountServ.getDataOfferDiscountList();


    setTimeout(() => {
      console.log('Async operation has ended');

      event.target.complete();
    }, 2000);
  }
}
