import { CUSTOM_ELEMENTS_SCHEMA } from '@angular/core';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PlanCalendarPage } from './plan-calendar.page';

describe('PlanCalendarPage', () => {
  let component: PlanCalendarPage;
  let fixture: ComponentFixture<PlanCalendarPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PlanCalendarPage ],
      schemas: [CUSTOM_ELEMENTS_SCHEMA],
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PlanCalendarPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
