import { Component, OnInit } from '@angular/core';
import { Router, NavigationExtras, ActivatedRoute } from '@angular/router';
import { SocialSharing } from '@ionic-native/social-sharing/ngx';
import { Platform } from '@ionic/angular';
import {GlobalService} from "../../services/global.service";

@Component({
  selector: 'app-plan-create-success',
  templateUrl: './plan-create-success.page.html',
  styleUrls: ['./plan-create-success.page.scss'],
})
export class PlanCreateSuccessPage implements OnInit {
  plan: any;
  public successUrl: string = "";

  constructor(private router: Router, private route: ActivatedRoute, private socialSharing: SocialSharing, private platform: Platform, public _globalServ: GlobalService) {
    this.route.queryParams.subscribe((params: any) => {
      if (this.router.getCurrentNavigation().extras.state) {
        this.plan = this.router.getCurrentNavigation().extras.state.title;
      }
    });
      if (this.platform.is('ios')) {
          this.successUrl = "https://apps.apple.com/us/app/vive-ie/id1469662914?ls=1";
      }
      if (this.platform.is('android')) {
          this.successUrl = "https://play.google.com/store/apps/details?id=com.vistingo.vive";
      }
  }

  ngOnInit() {
  }

  continue() {
    this.router.navigate(['tabs/plan-list']);
  }
  share(to) {

    if (to == "whatsapp") {
      this.socialSharing.shareViaWhatsApp('Hi! Download VIVE IE app and join my plan ' + this.plan, null, this.successUrl).then((res) => {
        // Success!
        console.log(res)
      }).catch((err) => {
        // Error!
        console.log(err)
      });
    }
    if (to == "messanger") {
      this.socialSharing.shareVia("com.facebook.orca", 'Hi! Download VIVE IE app and join my plan ' + this.plan, null,null, this.successUrl).then((res) => {
        // Success!
        console.log(res)
      }).catch((err) => {
        // Error!
        console.log(err)
      });
    }
    if (to == "sms") {
      this.socialSharing.shareViaSMS('Hi! Download VIVE IE app and join my plan ' + this.plan + ' ' + this.successUrl, null).then((res) => {
        // Success!
        console.log(res)
      }).catch((err) => {
        // Error!
        console.log(err)
      });
    }

  }
  invitePeople() {
    this.router.navigate(['group-create-invite']);
  }
}
