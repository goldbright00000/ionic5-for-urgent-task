import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { Routes, RouterModule } from '@angular/router';
import { Ionic4DatepickerModule } from
    '@logisticinfotech/ionic4-datepicker';

import { IonicModule } from '@ionic/angular';

import { PlanCreatePage } from './plan-create.page';

import { ComponentModule } from '../../components/component.module';
import { AutocompletePageModule } from '../autocomplete/autocomplete.module';

const routes: Routes = [
  {
    path: '',
    component: PlanCreatePage
  }
];

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ComponentModule,
	IonicModule,
	AutocompletePageModule,
    RouterModule.forChild(routes),
    Ionic4DatepickerModule
  ],
  declarations: [PlanCreatePage]
})
export class PlanCreatePageModule {}
