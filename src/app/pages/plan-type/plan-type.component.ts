import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';

@Component({
  selector: 'app-plan-type',
  templateUrl: './plan-type.component.html',
  styleUrls: ['./plan-type.component.scss'],
})
export class PlanTypeComponent implements OnInit {
  public planTypeValue: any;

  constructor(private storage: Storage) { 
    this.storage.get('planType').then((val) => {
      if (val && val != -1) {
        this.planTypeValue = val;
      } else {
        this.planTypeValue = 1;
      }
      this.storage.set('planType', this.planTypeValue);
    });
  }

  ngOnInit() {
  }

  changePlan() {
    console.log('Plan Type', this.planTypeValue)
    this.storage.set('planType', this.planTypeValue);
  }

}
