import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { ComponentModule } from '../../components/component.module';

import {PlanTypeComponent} from './plan-type.component'


const routes: Routes = [
  {
    path: '',
    component: PlanTypeComponent
  }
];

@NgModule({
  declarations: [PlanTypeComponent],
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ComponentModule,
    RouterModule.forChild(routes)
  ],
})
export class PlanTypeModule { }
