import { Component, OnInit } from '@angular/core';
import { GlobalService } from '../../services/global.service';
import {
	ToastController, NavController
} from '@ionic/angular';

@Component({
	selector: 'app-price',
	templateUrl: './price.page.html',
	styleUrls: ['./price.page.scss'],
})
export class PricePage implements OnInit {
	price: any;
	free: any = false;
	valid: any = true;
	constructor(
		public _globalServ: GlobalService,
		public navCtrl: NavController,
		private toastController: ToastController, ) {
		this.price = this._globalServ.price
		if (!this._globalServ.price)
			this.free = true;
	}

	ngOnInit() {
	}
	get() {
		console.log(this.price)
		this._globalServ.price = this.price;
		if (!this.price) {
			this.free = true;
		} else {
			this.free = false;
		}
	}

	getFree() {
		console.log(this.free);
		if (this.free == true) {
			this._globalServ.price = undefined;
			this.price = undefined;
		}

	}

	_keyUp() {
		const pattern = /[a-zA-z\+\-\ ]/;

		let inputChar = String.fromCharCode(this.price);
		console.log(this.price);
		if (this.price.match(/[a-zA-Z]+$/)) {
			console.log("notvalid")
			this.valid = false;
		} else {
			console.log("valid")
			this.valid = true;
		}
	}

	async goBack() {
		if (!this.valid) {
			const toast = await this.toastController.create({
				message: "please enter valid price",
				duration: 2000
			});
			toast.present();
		}
	}
}
