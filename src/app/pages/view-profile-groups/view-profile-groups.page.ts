import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { GlobalService } from '../../services/global.service';
import { environment } from '../../../environments/environment';
import { get } from 'lodash';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-view-profile-groups',
  templateUrl: './view-profile-groups.page.html',
  styleUrls: ['./view-profile-groups.page.scss'],
})
export class ViewProfileGroupsPage implements OnInit {

  Participations: any;
  url: any = environment.apiUrl;
  firstName: any = '';
  userId: any = '';
  groups: any = [];
  groupCheck: boolean;

  constructor(
    private _userServ: UserService,
    private _globalServ: GlobalService,
    private actRout: ActivatedRoute
  ) {
      this.actRout.queryParams.subscribe(data=>{
          console.log(data.id);
          if(data.id){
              this.userId = data.id;
          }
          this.getProfile();
          this.getGroups();
      })
  }

  ngOnInit() {
  }

  getGroups() {
      let id = this.userId ? this.userId : this._globalServ.idUser;
    this._userServ.getUserGroups(id).subscribe(data => {
      this.groups = get(data, 'group', []);
      this.groupCheck = this.groups.length ? true : false;
    }, err => { 
      console.log('err user groups: ', err);
    });
  }

    getProfile() {
        if(this.userId === undefined || this.userId === ''){
            this._userServ.getProfile(this._globalServ.idUser, { 'entity': this._globalServ.org })
                .subscribe(dataProf => {
                    this.firstName = get(dataProf, 'user.first_name', '');
                }, error => { console.log(error); })
        }else{
            this._userServ.getProfile(this.userId, { 'entity': this._globalServ.org })
                .subscribe(dataProf => {
                    var name = get(dataProf, 'user.name', '');
                    var firstName = name.split(' ');
                    this.firstName = firstName[0];
                }, error => { console.log(error); })
        }
    }
}
