import { Component, OnInit, ViewChild } from '@angular/core';
import { UserService } from '../../services/user.service';
import { GlobalService } from '../../services/global.service';
import { environment } from '../../../environments/environment';
import { get } from 'lodash';
import { ActivatedRoute, NavigationExtras } from '@angular/router';
import { GroupService } from 'src/app/services/group.service';
import { ToastController, Platform, IonInput, ActionSheetController, IonContent } from '@ionic/angular';
import { PlanService } from 'src/app/services/plan.service';
import { Crop } from '@ionic-native/crop/ngx';
import { Camera, CameraOptions } from '@ionic-native/camera/ngx';
import { DomSanitizer } from '@angular/platform-browser';
import { WebView } from '@ionic-native/ionic-webview/ngx';
import { FileTransfer, FileUploadOptions, FileTransferObject } from '@ionic-native/file-transfer/ngx';
import * as moment from 'moment';

@Component({
  selector: 'app-write-comment',
  templateUrl: './write-comment.page.html',
  styleUrls: ['./write-comment.page.scss'],
})
export class WriteCommentPage implements OnInit {
  @ViewChild(IonContent) content: IonContent;
  @ViewChild('comment_box') coment: IonInput;
  Participations: any;
  url: any = environment.apiUrl;
  commentData: any = '';

  groupId: any = '';
  comments: any = [];
  profile: any = {};
  type: any;
  tempDate;

  tempCommentData: any = [];
  pageTitleName: any = false;
  pathImage: any;
  pathImageCropper: any;
  pathImagePost: any;
  commentVisible: boolean = false;
  PostCommentData: never;
  like: boolean = false;

  done: boolean = true;

  constructor(
    private camera: Camera,
    private sanitizer: DomSanitizer,
    private crop: Crop,
    private webview: WebView,
    private actionSheetController: ActionSheetController,
    private _userServ: UserService,
    public _globalServ: GlobalService,
    private _groupServ: GroupService,
    private actRoute: ActivatedRoute,
    private toastController: ToastController,
    private platform: Platform,
    private _planServ: PlanService,
    private transfer: FileTransfer
  ) {
    if (this.actRoute.snapshot.queryParams.data != null) {
      let getParams = JSON.parse(this.actRoute.snapshot.queryParams.data);
      this.type = JSON.parse(this.actRoute.snapshot.queryParams.type).type;
      this.groupId = getParams.id;
      console.log('groupId: ', this.groupId, " type =>", this.type);
      this.getProfile();
    }

    if (this.platform.is('android')) {
      window.addEventListener('keyboardWillShow', (event: any) => {
        console.log("keyboard show", event)
        document.getElementById('type-bar').style.marginBottom = (90 + event.keyboardHeight) + 'px';
      })

      window.addEventListener('keyboardDidHide', (event: any) => {
        console.log("keyboard show", event)
        document.getElementById('type-bar').style.marginBottom = 'unset';
      })
    }

  }

  ngOnInit() {
    console.log("ionViewDidLoad");
    setTimeout(() => {
      this.coment.setFocus();
    }, 500);
    this.tempDate = moment();
  }


  logScrolling(e) {
    if (e.detail.scrollTop > 50) {
      this.pageTitleName = true;
    } else {
      this.pageTitleName = false;
    }
  }


  selectFile(source) {
    const options: CameraOptions = {
      quality: 100,
      destinationType: this.camera.DestinationType.FILE_URI,
      encodingType: this.camera.EncodingType.JPEG,
      mediaType: this.camera.MediaType.PICTURE,
      sourceType: source
    }

    this.camera.getPicture(options).then((imageData) => {
      this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(imageData));

      this.pathImageCropper = imageData;
      this.pathImagePost = imageData;
      this._globalServ.PostImagePost = imageData;
      this._globalServ.PostImagePath = this.pathImage;
      this._globalServ.planImagePost = imageData;
			this._globalServ.planImagePath = this.pathImage;
      this.cropFunc();
    }, (err) => {
      console.log(err);
    });
  }

  cropFunc() {
    this.crop.crop(this.pathImageCropper, { quality: 75 })
      .then(
        newImage => {
          this.pathImage = this.sanitizer.bypassSecurityTrustUrl(this.webview.convertFileSrc(newImage));
          this.pathImageCropper = newImage;
          this.pathImagePost = newImage;
          this._globalServ.PostImagePost = newImage;
          this._globalServ.PostImagePath = this.pathImage;
          this._globalServ.planImagePost = newImage;
          this._globalServ.planImagePath = this.pathImage;

        },
        error => console.error('Error cropping image', error)
      );
  }
  async showAlert(msg) {
    const toast = await this.toastController.create({
      message: msg,
      duration: 2000
    });
    toast.present();
  }


  onPostingComment(tempdata, i) {
    tempdata.tempdata.push({ "comment": tempdata.text, comm_date: new Date() })
    tempdata.text = '';
  }

  async goToselectImage() {
    let navigationExtras: NavigationExtras = {
      queryParams: {
        data: JSON.stringify({ 'plan': true })
      }
    };
    let titleLabel;
    let cancelLabel;
    let EditLabel;
    let ChangeLabel;
    let RemoveLabel;
    if (this._globalServ.language == 'en') {
      titleLabel = 'Photo';
      cancelLabel = 'Cancel';
      EditLabel = 'Edit Photo';
      ChangeLabel = 'Change Photo';
      RemoveLabel = 'Remove Photo';
    }
    if (this._globalServ.language == 'es') {
      titleLabel = '';
      cancelLabel = 'Cancelar';
      EditLabel = 'Editar foto';
      ChangeLabel = 'Cambiar Fotografía';
      RemoveLabel = 'Eliminar foto'
    }


    const actionSheetForPlan_group = await this.actionSheetController.create({
      header: titleLabel,

      buttons: [
        {
          text: EditLabel,

          handler: () => {
            this.cropFunc();
          }
        },
        {
          text: ChangeLabel,
          handler: () => {
            this.selectFile(2)
          }
        },
        {
          text: RemoveLabel,
          handler: () => {
            this._globalServ.PostImagePath = null;
          }
        },
        {
          text: cancelLabel,

          role: 'cancel',
          handler: () => {
            console.log('Cancel clicked');
          }
        }
      ]
    }).then(actionsheet => {
      actionsheet.present();
    });;
  }

  onComment(comment, i) {
    comment.visible = true;
  }

  getProfile() {
    this._userServ.getPublicProfile(this._globalServ.idUser).subscribe(data => {
      this.profile = get(data, '[0]', {});
      console.log('this.profile: ', this.profile);
      if (this.type == 'group') {
        this.getGroupComments();
      } else {
        this.getPlanComment();
      }
    }, err => {
      console.log('err public profile: ', err);
    });
  }

  getDuration(tcreated) {
    let now = new Date().getTime();
    let created = new Date(tcreated).getTime();

    if (now > created) {
      let hours = new Date(now - created).getHours();
      if (Math.floor(hours / 24) === 0) {
        return '1d';
      }
      return Math.floor(hours / 24) + 'd';
    } else {
      return '';
    }
  }

  getGroupComments() {
    this._groupServ.getGroupComments(this.groupId).subscribe(data => {

      console.log('group comment data: ', data);
      this.comments = get(data, 'comment', {});

      this.comments.forEach(element => {
        element.visible = false;
        element.tempdata = [{ "comment": "hello lorem porem there we re ", comm_date: new Date()},{ "comment": "fdb bf lorem porem there we re ", comm_date: new Date()}];
        element.text = ''
      });
      this.comments = this.comments.map(o => {
        let duration = this.getDuration(o.created);

        return {
          ...o,
          duration
        };
      })
    }, err => {
      console.log('group comment err: ', err);
    });
  }

  getPlanComment() {
    this._planServ.getItemUniv(this.groupId)
      .subscribe(data => {
        console.log('group comment data: ', data);
        this.comments = data['comments'];
      })
  }

  addGroupComment() {
    this.done = false;

    let param = {
      userId: this._globalServ.idUser,
      email: this.profile.email,
      comment: this.commentData
    };

    console.log('param: ', param);

    if (this._globalServ.planImagePost) {
      const fileTransfer: FileTransferObject = this.transfer.create();
			let filename = 'planImage_' + Math.random() * 100000000000000000;
			let options: FileUploadOptions = {
				fileKey: 'image',
				fileName: filename + '.jpg',
				params: param,
      }
      let endpoint = 'group-add-comment-with-photo/' + this.groupId;
			console.log(this._globalServ.planImagePost)
			console.log(environment.apiUrl)
			this._globalServ.planImagePost = this._globalServ.planImagePost ? this._globalServ.planImagePost : "";
			fileTransfer.upload(this._globalServ.planImagePost, environment.apiUrl + `/${endpoint}`, options)
				.then(data => {
					console.log('<<<<posted group comment successfully !!!>>>>');
					this._globalServ.planImagePath = null;
					this._globalServ.planImagePost = null;
					console.log('add comment sucess : ', data);
          this.commentData = '';
          this._globalServ.PostImagePath = null;
          this.getGroupComments();
          this.content.scrollToBottom();
          this.done = true;
				}, (err) => {
					console.log('add comment err : ', err);
				});
    } else {
      this._groupServ.addComment(this.groupId, param).subscribe(async data => {
        console.log('add comment sucess : ', data);
        const toast = await this.toastController.create({
          message: 'Added comment successfully.',
          duration: 2000
        });
        toast.present();
  
        this.commentData = '';
        this.getGroupComments();
        this._globalServ.PostImagePath = null;
        this.content.scrollToBottom();
        this.done = true;
      }, async err => {
        console.log('add comment err : ', err);
        const toast = await this.toastController.create({
          message: err.message,
          duration: 2000
        });
        toast.present();
        this.commentData = '';
        this._globalServ.PostImagePath = null;
        this.content.scrollToBottom();
        this.done = true;
      });
    }
  }

  addPlanComment() {
    let param = {
      fk_ie_plan_id: this.groupId,
      author: this._globalServ.idUser,
      comment: this.commentData
    }

    console.log("plan =>", param);

    this._planServ.addPlanComment(param).subscribe(async data => {
      const toast = await this.toastController.create({
        message: 'Added comment successfully.',
        duration: 2000
      })
      toast.present();
      this.commentData = ''
      this.getPlanComment();
    }, async err => {
      console.log('add comment err : ', err);
      const toast = await this.toastController.create({
        message: err.message,
        duration: 2000
      });
      toast.present();
      this.commentData = '';
    })
  }

  onLike(){
    this.like = !this.like;
  }

  timeDiff(mytime) {
   // console.log("---", mytime);
    var t2 = mytime;
    let time2 = moment(t2);

    var duration1 = this.tempDate.diff(time2, 'seconds');

    if (duration1 > 60) {
      duration1 = this.tempDate.diff(time2, 'minutes');
      if (duration1 > 60) {
        duration1 = this.tempDate.diff(time2, 'hours');
        if (duration1 > 24) {
          duration1 = this.tempDate.diff(time2, 'days');
          if (duration1 > 7) {
            duration1 = this.tempDate.diff(time2, 'weeks');
            if (duration1 > 5) {
              duration1 = this.tempDate.diff(time2, 'month');
              if (duration1 > 12) {
                duration1 = this.tempDate.diff(time2, 'years');
                return duration1 + "y";
              } else {
                return duration1 + "m";
              }
            } else {
              return duration1 + "w";
            }
          } else {
            return duration1 + "d";
          }
        } else {
          return duration1 + "h";
        }
      } else {
        return duration1 + "min";
      }
    }
    else {
      if (duration1 < 0) {
        return "0s";
      }
      else {
        return duration1 + "s";
      }

    }
  }

}
