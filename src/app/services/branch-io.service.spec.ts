import { TestBed } from '@angular/core/testing';

import { BranchIOService } from './branch-io.service';

describe('BranchIOService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: BranchIOService = TestBed.get(BranchIOService);
    expect(service).toBeTruthy();
  });
});
