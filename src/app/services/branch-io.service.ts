import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class BranchIOService {

  constructor(private router: Router) {

  }
    

  generateShortURL() {
    return new Promise<any>((resolve,reject)=>{
      const Branch = window["Branch"];
      var properties = {
        canonicalIdentifier: this.router.url,     
      };
      var branchUniversalObj = null;
      Branch.createBranchUniversalObject(properties)
      .then(function(res) {
        var branchUniversalObj = res;        
        branchUniversalObj
        .generateShortUrl({}, {})
        .then(function(res) {          
          resolve(res);          
        })
        .catch(function(err) {          
          reject(err);
        });    
      })
      .catch(function(err) {        
          reject(err);
      });
    });    
  }
}
