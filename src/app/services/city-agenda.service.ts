import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { GlobalService } from '../services/global.service';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CityAgendaService {

  constructor(
    private http: HttpClient,
    private _globalServ: GlobalService,
  ) { }

  getFilteredEvents(id: any) {
    return this.http.get(environment.apiUrl + '/app-filtered-city-agenda/' + id + '?preferences=' + this._globalServ.currentPreferencesForAgenda);
  }

  getItem(id: any) {
    return this.http.get(environment.apiUrl + '/get-event/' + id);
  }

  getDataCityAgendaList() {
    this.getFilteredEvents(this._globalServ.idUser)
      .subscribe((data: any) => {
          data.events.sort((a, b) => {
              const keyA = new Date(a.valid_until),
                  keyB = new Date(b.valid_until);
              if (keyA < keyB) return -1;
              if (keyA > keyB) return 1;
              return 0;
          });
          this._globalServ.listCityAgenda = data;
      }, error => {
        console.log(error);
      })
  }

}
