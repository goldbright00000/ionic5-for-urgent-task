import { Injectable } from '@angular/core';

@Injectable({
	providedIn: 'root'
})
export class GlobalService {
	cityGiude: any;

	invited: any;
	select: any;
	androidDivice: boolean;
	iosDivice: boolean;
	idUser: any;
	//org: any = 'CEU';
	org: any = 'IE';
	language: any;

	//save image profile
	profileImagePath: any;

	//create plan
	planImagePath: any;
	planImagePost: any;
	idsCategories: any;


	PostImagePath: any;
	PostImagePost: any;

	//create plan
	groupImagePath: any;
	groupImagePost: any;

	//data of tabs
	//plan
	listPlanHighlight: any;
	listPlanFiltered: any;
	//City Guide
	listCityGuide: any;
	//City agenda
	listCityAgenda: any;
	//offer and discount
	listOfferAndDisc: any;
	createdBy: any;
	category: any = [];
	price: any;
	planType: any;
	groupCat: any = [];
	categoryCount: any = 0;
	currentPreferences: any;
	filtersArry: any;
	cityFilterCate: any = [];
	availableSeats: any;
	timesLogin: any;
	getParticipants: any;
	dataUser: any;
	cityagenda: any;
	//*** saved in storage ***
	//token
	//idUser
	//language
	firstName: string;
	lastName: string;
	currentPreferencesForAgenda: any;
	showIcon: boolean = true;

	grouptype: any = 'public';
	groupadmin: any;

	constructor() { }
}
